var SupportManager = function(){

	this.view = {
		form: $("#support-contact-form"),
		form_error: $("#support-contact-form .form-error"),
		form_inner: $("#support-contact-form-content"),
		sent_model: $("#hidden-content #contact-sent"),
		loader_model: $("#hidden-content .card-loader")
	};

	this.showCompleted = function(){
		this.view.form.html("");
		this.view.sent_model.clone().appendTo(this.view.form);
		alert("Check the spam folder in your email")
	};

	this.showLoader = function(){
		this.view.form_inner.hide();
		this.view.loader_model.clone().appendTo(this.view.form);
	};
	this.hideLoader = function(){
		this.view.form_inner.show();
		this.view.form.find(".card-loader").hide();
	};

	this.showError = function(message){
		this.view.form_error.find("p").text(message);
		this.view.form_error.show();
	};
	this.hideError = function(){
		this.view.form_error.hide();  
	};

	this._request = function(username, email, subject, message, callback, onerror){
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		$.ajax({
			url: "/mail",
			method: "POST",
			data: {
				'username': username,
				'email': email,
				'subject': subject,
				'message': message
			},
			datatype: 'html'
		}).done(callback).fail(onerror);
	};
	this.request = function(){
		function requestCallback(data){
			this.hideLoader();
			if(data === 'true'){
				this.showCompleted();
			} else {
				this.showError(data.msg);
			}
		}
		function requestError(error){
			this.hideLoader();
			console.log(error);
			this.showError("Error. Retry later");
		}

		var username = this.view.form.find("#form-name-input").val(),
			email = this.view.form.find("#form-email-input").val(),
			subject = this.view.form.find("#form-subject-select").val(),
			message = this.view.form.find("#form-message-input").val();

		if(!username){
			this.showError("Write instagram username");
		} else if(!email){
			this.showError("Enter email");
		} else if(!message){
			this.showError("Write mesage");
		} else {
			this.hideError();
			this.showLoader();
			this._request(username, email, subject, message, 
				requestCallback.bind(this), requestError.bind(this));
		}

	};

	this.initListeners = function(){
		this.view.form.on("submit", function(e){
			e.preventDefault();
			this.request();
		}.bind(this));
	};
	this.init = function(){

		this.initListeners();
		return this;
	}
};
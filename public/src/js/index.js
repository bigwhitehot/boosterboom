var Animator = function(){
	var how_step_one = {
			name: "how_step_one",
			offset: function(){
			    var landingHowStepOne = $('#landing-how-step-one');
				return landingHowStepOne.offset().top - window.innerHeight / 2;
			},
			action: function(){
				var animation_root = $("#landing-how-step-one");
				animation_root.addClass("animation-one");
				setTimeout(function(){
					animation_root.addClass("animation-two");
				}, 500);
				setTimeout(function(){
					animation_root.addClass("animation-three");
				}, 600);
				setTimeout(function(){
					animation_root.addClass("animation-four");
				}, 600);
				setTimeout(function(){
					animation_root.addClass("animation-five");
				}, 900);
			}
		},
		how_step_two = {
			name: "how_step_two",
			offset: function(){
				return $("#landing-how-step-two").offset().top - window.innerHeight / 2;
			},
			action: function(){
				var animation_root = $("#landing-how-step-two")
				animation_root.addClass("animation-one");
				setTimeout(function(){
					animation_root.addClass("animation-two");
				}, 500);
				setTimeout(function(){
					animation_root.addClass("animation-three");
				}, 600);
				setTimeout(function(){
					animation_root.addClass("animation-four");
				}, 600);
				setTimeout(function(){
					animation_root.addClass("animation-five");
				}, 900);

			}
		},
		how_step_three = {
			name: "how_step_hree",
			offset: function(){
				return $("#landing-how-step-three").offset().top - window.innerHeight / 2;
			},
			action: function(){
				var animation_root = $("#landing-how-step-three")
				animation_root.addClass("animation-one");
				setTimeout(function(){
					animation_root.addClass("animation-two");
				}, 500);
				setTimeout(function(){
					animation_root.addClass("animation-three");
				}, 600);
				setTimeout(function(){
					animation_root.addClass("animation-four");
				}, 600);
				setTimeout(function(){
					animation_root.addClass("animation-five");
				}, 900);

			}
		};

	this.animations = [
		how_step_one,
		how_step_two,
		how_step_three
	];

	this.tryHooks = function(y){
		var fired = [];
		//Loop throught Animator objects 
		for(var i = 0; i < this.animations.length; i++){
			if(this.animations[i].offset() < y){
				this.animations[i].action();
				fired.push(this.animations[i].name);
			}
		}
		//Clean this.animations from objects that have been fired
		//done after the initial loop to avoid messing up indexes
		for(var i = 0; i < fired.length; i++){
			for(var j = 0; j < this.animations.length; j++){
				if(fired[i] == this.animations[j].name){
					this.animations.splice(j, 1);
					break;
				}
			}
		}

	}
	this.initListeners = function(){
		this.tryHooks($(window).scrollTop());
		$(window).on("scroll", function(){
			this.tryHooks($(window).scrollTop());
		}.bind(this));
	}
	this.init = function(){
		this.initListeners();
		return this;
	}
};
<?php

return [

    /*
     * unitpay.ru PUBLIC KEY for project
     */
    'UNITPAY_PUBLIC_KEY' => '256551-a9e3c',

    /*
     * unitpay.ru SECRET KEY for project
     */
    'UNITPAY_SECRET_KEY' => '24c915bad87385b6c9b1ee663ae54a57',

    /*
     * locale for payment form
     */
    'locale' => 'en',  // ru || en

    /*
     * Hide other payment methods
     */
    'hideOtherMethods' => 'false',

    /*
     *  SearchOrderFilter
     *  Search order in the database and return order details
     *  Must return array with:
     *
     *  orderStatus
     *  orderCurrency
     *  orderSum
     */
    'searchOrderFilter' => null, //  'App\Http\Controllers\ExampleController::searchOrderFilter',

    /*
     *  PaidOrderFilter
     *  If current orderStatus from DB != paid then call PaidOrderFilter
     *  update order into DB & other actions
     */
    'paidOrderFilter' => null, //  'App\Http\Controllers\ExampleController::paidOrderFilter',

    'payment_forms' => [
        'cards' => true,
        'yandex' => false,
        'qiwi' => true,
        'cash' => true,
        'webmoney' => false,
    ],

    // Allowed ip's http://help.unitpay.ru/article/67-ip-addresses
    'allowed_ips' => [
        '31.186.100.49',
        '178.132.203.105',
        '52.29.152.23',
        '52.19.56.234',
    ],

    /*
     * The notification that will be send when payment request received.
     */
    'notification' => \ActionM\UnitPay\UnitPayNotification::class,

    /*
     * The notifiable to which the notification will be sent. The default
     * notifiable will use the mail and slack configuration specified
     * in this config file.
     */
    'notifiable' => \ActionM\UnitPay\UnitPayNotifiable::class,

    /*
     * By default notifications are sent always. You can pass a callable to filter
     * out certain notifications. The given callable will receive the notification. If the callable
     * return false, the notification will not be sent.
     */
    'notificationFilter' => null,

    /*
     * The channels to which the notification will be sent.
     */
    // 'channels' => ['mail', 'slack'],
    'channels' => ['mail'],

    'mail' => [
        'to' => 'boosterboomservice@gmail.com',  // your email
    ],

    'slack' => [
        'webhook_url' => 'slack', // slack web hook to send notifications
    ],
];

<?php

use Illuminate\Support\Facades\Route;
use App\Models\PageModel;
use App\Models\MenuModel;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|*/

Route::get('/', 'Controller@welcome')->name('/');

Route::post('proxy/user/validate-token', 'Controller@validateToken');
Route::get('payment/success', 'Controller@paymentSuccess');
Route::get('payment/cancel', 'Controller@paymentCancel');

Route::get('/unitpay/result', 'UnitPayController@payOrderFromGate');
Route::get('/test-pay', 'UnitPayController@testPay');

// Route::match(array('get', 'post'), 'checkout', 'PaymentController@payWithpaypal')->name('checkout');
Route::post('checkout/', 'Controller@proxy')->name('checkout');
Route::get('checkout/{type?}/{count?}', 'Controller@orderConstructor');
// Route::get('status', 'PaymentController@getPaymentStatus')->name('status');
// Route::get('success', 'PaymentController@successPayment')->name('success');

//Route::post('/mail', 'Controller@messageController');
Route::post('/mail', function() {
    return "Coming soon";
});

Route::get('/home', 'Controller@adsHome');

Route::get('/faq', "Controller@showFaqView");
Route::get('/privacy-policy', function() {
    return view('privacyPolicy');
});
Route::get('/terms', function() {
    return view('termsOfService');
});


Route::post('/proxy', 'Controller@proxy');
Route::get('/testing', 'Test@test');

Route::get('/{url?}', function ($url) {
    if ($url == "index") {
        return redirect("/");
    }

    $page = PageModel::where('url', $url)->firstOrFail();
    $page->setPageNameAttribute($url);
    return view('page', [
        "page" => $page,
        "menu" => MenuModel::all()
    ]);
});
@section('how-it-work')
    <div id="landing-how">
        <div id="landing-how-header">
            <h2>How does this work?</h2>
            <h3>Three Simple Steps</h3>
        </div>
        <div id="landing-how-content">
            <div id="landing-how-step-one" class="landing-how-step animation-one animation-two animation-three animation-four animation-five">
                <div class="step-number">l</div>
                <div class="step-description">
                    <h3>Choose Package</h3>
                    <p>Choose what suits you best! We offer a variety of packages for groundbreaking prices and best yet, all purchases are delivered instantly to ensure that you get your activity boost on time and secure a top spot on Instagram.</p>
                </div>
                <div class="step-illustration">
                    <img id="icon-human" class="icon" alt="Choose Package" title="Choose Package" src="/src/img/icon-human.svg">
                    <img id="icon-heart" class="icon" alt="Choose Package" title="Choose Package" src="/src/img/icon-heart.svg">
                    <img id="icon-video" class="icon" alt="Choose Package" title="Choose Package" src="/src/img/icon-video.svg">
                    <img id="icon-clock" class="icon" alt="Choose Package" title="Choose Package" src="/src/img/icon-clock.svg">
                    <img id="icon-location" class="icon" alt="Choose Package" title="Choose Package" src="/src/img/icon-location.svg">
                </div>
            </div>
            <div id="landing-how-step-two" class="landing-how-step animation-one animation-two animation-three animation-four animation-five">
                <div class="step-number">ll</div>
                <div class="step-illustration">
                    <img id="icon-cloud" class="icon" alt="Checkout" title="Checkout" src="/src/img/icon-cloud.svg">
                    <img id="icon-doodle" class="icon" alt="Checkout" title="Checkout" src="/src/img/icon-doodle.svg">
                    <img id="icon-send" class="icon" alt="Checkout" title="Checkout" src="/src/img/icon-send.svg">
                    <img id="icon-circle-1" class="icon" alt="Checkout" title="Checkout" src="/src/img/icon-circle.svg">
                    <img id="icon-circle-2" class="icon" alt="Checkout" title="Checkout" src="/src/img/icon-circle.svg">
                    <img id="icon-circle-3" class="icon" alt="Checkout" title="Checkout" src="/src/img/icon-circle.svg">
                </div>
                <div class="step-description">
                    <h3>Checkout</h3>
                    <p>There’s no registration process and we won’t ask for your Instagram password. Just enter your username, email and go to the next section to confirm your order</p>
                </div>
            </div>
            <div id="landing-how-step-three" class="landing-how-step animation-one animation-two animation-three animation-four animation-five">
                <div class="step-number">lll</div>
                <div class="step-description">
                    <h3>See Results</h3>
                    <p>You’ll start watching first results just in 1 hour after the purchase. Your Instagram account will be blowing up with activity with the help of BoosterBoom! </p>
                </div>
                <div class="step-illustration">
                    <img id="icon-doodle-1" class="icon" alt="See Results" title="See Results" src="/src/img/icon-doodle.svg">
                    <img id="icon-doodle-2" class="icon" alt="See Results" title="See Results" src="/src/img/icon-doodle.svg">
                    <img id="icon-plus" class="icon" alt="See Results" title="See Results" src="/src/img/icon-plus.svg">
                    <img id="icon-play" class="icon" alt="See Results" title="See Results" src="/src/img/icon-play.svg">
                    <img id="icon-heart-results" class="icon" alt="See Results" title="See Results" src="/src/img/icon-heart-thin.svg">
                    <img id="icon-comment" class="icon" alt="See Results" title="See Results" src="/src/img/icon-comment.svg">
                </div>
            </div>
        </div>
    </div>
@show
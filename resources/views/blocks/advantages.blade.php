@section('advantages')
    <div class="buy-x-features-prices" id="advantages" style="background-image:url('/src/img/background_f.svg');background-size:contain;">
        <div class="buy-x-features">
        <div class="buy-x-features-header">
            <h2>Advantages</h2>
        </div>
        <div class="buy-x-features-content">
            <div id="followers-instant-feature" class="features-item">
                <div class="features-illustration">
                    <div id="followers-instant-circle"></div>
                    <img id="followers-instant-1" class="illustration-part" alt="Instant Delivery" title="Instant Delivery" src="src/img/buy-followers-instant-notif-2.svg">
                    <img id="followers-instant-2" class="illustration-part" alt="Instant Delivery" title="Instant Delivery" src="src/img/buy-followers-instant-notif-3.svg">
                    <img id="followers-instant-3" class="illustration-part" alt="Instant Delivery" title="Instant Delivery" src="src/img/buy-followers-instant-notif-1.svg">
                </div>
                <h3>Safety guarantee</h3>
                <p>Tracking the speed of adding subscribers to avoid any problems</p>
            </div>
            <div id="followers-refill-feature" class="features-item">
                <div class="features-illustration">
                    <img id="followers-refill-3" class="illustration-part illustration-dot" alt="Automatic Followers Refill" title="Automatic Followers Refill" src="src/img/buy-followers-refill-chart.svg">
                    <img id="followers-refill-2" class="illustration-part illustration-dot" alt="Automatic Followers Refill" title="Automatic Followers Refill" src="src/img/buy-followers-refill-dot-1.svg">
                    <img id="followers-refill-1" class="illustration-part illustration-dot" alt="Automatic Followers Refill" title="Automatic Followers Refill" src="src/img/buy-followers-refill-dot-3.svg">
                </div>
                <h3>Restoring followers</h3>
                <p>If some part of the followers will be lost, we will restore them for free. To do this, just write to technical support</p>
            </div>
            <div id="followers-support-feature" class="features-item">
                <div class="features-illustration">
                    <div id="followers-support-circle"></div>
                    <img id="followers-support-1" class="illustration-part illustration-dot" alt="Customer Support" title="Customer Support" src="src/img/buy-followers-support-message-1.svg">
                    <img id="followers-support-2" class="illustration-part illustration-dot" alt="Customer Support" title="Customer Support" src="src/img/buy-followers-support-message-2.svg">
                    <img id="followers-support-3" class="illustration-rectangle illustration-part" alt="Customer Support" title="Customer Support" src="src/img/buy-followers-support-dot-1.svg">
                </div>
                <h3>Individual support</h3>
                <p>All questions are answered by our team. No stereotyped answers. Only individual approach</p>
            </div>
        </div>
        </div>
    </div>
 @show
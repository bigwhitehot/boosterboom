@section('top')
<div class="buy-x-v2-landing">
    <div class="buy-x-header menu-container">
        <div class="logo">
            <a href="/" title="To Homepage" style="margin-top: -16px;"><img src="/src/img/loogo.png" alt="logo" title="logo" width="auto">
                <p class="menu-container-logo-text" style="color: #fff;top: -20px;margin-left: 5px;display: inline;">BoosterBoom</p>
            </a>
        </div>

        <div id="menu-mobile-icon" class="buy-x-mobile-icon">
            <div class="dash"></div>
            <div class="dash"></div>
            <div class="dash"></div>
        </div>
        <menu id="menu-inner" >
            @foreach ($menu as $key => $menuItem)
                <a class="menu-item" href="{{$menuItem->link}}" title="{{$menuItem->name}}">
                    <span>{{$menuItem->name}}</span>
                    <div class="menu-animated-underline"></div>
                </a>
            @endforeach
        </menu>
    </div>
    @include('blocks.header')
</div>
@show

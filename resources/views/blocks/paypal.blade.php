<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<p id="form">
    Form
</p>
<form action="paypal">
    <input type="text" name="nickname" id="nickname">
    <input type="text" name="type">
    <input type="text" name="count">
    <input type="button" id="submit">
</form>
    
</body>
<script>
    var button = document.getElementById("submit");
    button.onclick = function () {
        var inpNickname = document.querySelector('input[name=nickname]');
        var inpType = document.querySelector('input[name=type]');
        var inpCount = document.querySelector('input[name=count]');
        var http = new XMLHttpRequest();
        var params = {
            'nickname': inpNickname.value,
            'type': inpType.value,
            'count': inpCount.value
        };
        http.open("POST", "/beta");
        http.setRequestHeader("Content-Type", "application/");
        http.send();
        console.log(http.responseText);
    };
</script>

</html>
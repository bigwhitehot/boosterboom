@section('header')
    @if($page->url == 'followers')
        @include('items.followers-header')
    @elseif($page->url == 'likes')
        @include('items.likes-header')
    @elseif($page->url == 'special')
        @include('items.special-header')
    @elseif($page->url == 'index')
        @include('items.index-header')
    @elseif($page->url == 'views')
        @include('items.views-and-comments-header')
    @endif
    <div class="buy-x-landing-scroll-down">
        <img id="scroll-down-icon" alt="Scroll Down" title="Scroll Down" src="src/img/scroll-down-2.svg">
    </div>
    <img class="buy-x-landing-curve" alt="Scroll Down" title="Scroll Down" src="src/img/landing-curve.svg">
@show
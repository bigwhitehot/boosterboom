@section('our-uniques')
    <div class="our-uniques">
        <div class="title module-title">
            <p style="position: relative; z-index: 1">Our benefits</p>
        </div>

        <div class="uni-item">
            <div class="item-img">
                <img alt="No Sign-up No Hassle" class="pc"
                    data-src="/src/img/our-uniques-img-1.png"
                    src="/src/img/our-uniques-img-1.png"
                    lazy="loaded">
            </div>
            <div class="item-right">
                <div class="tgb16 item-right-title">
                    No Sign-up
                </div>
                <p class="item-des tgm12">
                    No sign-up and registration needed. Our service allows shorten expenses of time and prompt payment via PayPal without any hidden fees.
                </p>
            </div>
        </div>
        <div class="uni-item">
            <div class="item-right margin-diff">
                <div class="tgb16 item-right-title ">
                    Quality with Quantity
                </div>
                <p class="item-des tgm12">
                    The number doesn’t affect the quality. With the purchase every follower is verified by our special system that warranties bot followers’ absence. 
                </p>
            </div>
            <div class="item-img item-img2">
                <img alt="Quality with Quantity" class="pc"
                    data-src="/src/img/our-uniques-img-2.png"
                    src="/src/img/our-uniques-img-2.png"
                    lazy="loaded">
            </div>
        </div>
        <div class="uni-item">
            <div class="item-img item-img3">
                <img alt="Real-time Report" class="pc"
                    data-src="/src/img/our-uniques-img-3.png"
                    src="/src/img/our-uniques-img-3.png"
                    lazy="loaded">
            </div>
            <div class="item-right margin-diff">
                <div class="tgb16 item-right-title ">
                    Real-time Report
                </div>
                <p class="item-des tgm12">
                    Real-time tracking of your Instagram followers. Take control of your fan base and engage with our intelligent Instagram followers tracker.
                </p>
            </div>
        </div>
        <div class="uni-item last-one">
            <div class="item-right margin-diff">
                <div class="tgb16 item-right-title ">
                    Timely Support
                </div>
                <p class="item-des tgm12">
                    Responsive support team to help you get the most out of your service BoosterBoom. We also offer a money-back guarantee if you’re not satisfied.
                </p>
            </div>
            <div class="item-img item-img4">
                <img alt="Dedicated Support" class="pc"
                    data-src="/src/img/our-uniques-img-4.png"
                    src="/src/img/our-uniques-img-4.png"
                    lazy="loaded">
            </div>
        </div>
    </div>
@show
@section("category")
    <div id="category">
        <category
                categories="{{!empty($categories) ? $categories : ''}}"
                page-url="{{!empty($page) ? $page->url : ''}}"
                list-advantages="{{$advantages}}" >
        </category>
    </div>
@show
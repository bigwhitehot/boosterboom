@section("packageCategory")
    <div id="list-category" class="buy-x-features-prices" style="background-image:url('/src/img/background_f.svg');background-size:contain;">

        <div class="buy-x-header" style="padding-bottom: 20px;">
            <div class="buy-x-header-heading">
                <h2>Packages</h2>
            </div>
        </div>
        <div class="buy-x-packages-list a">
            @include('blocks.category')
        </div>
    </div>
@show
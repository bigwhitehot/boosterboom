@extends('layouts.app')
@section('content')
    <instagram
            type="{{!empty($type) ? $type : ''}}"
            count="{{!empty($count) ? $count : ''}}"
            special-offer="{{!empty($specialOffer) ? $specialOffer : ''}}"
            categories="{{!empty($categories) ? $categories : ''}}"
            paypal-route="{{secure_url('/checkout')}}"
            csrf-token="{{Session::token()}}">
    </instagram>
@stop
@section('how-to-make-an-order')
    <div class="buy-x-how" itemscope="" itemtype="http://schema.org/Product">
<div class="buy-x-how-header">
    <h2>How to make an order</h2>
    <p>The process of buying subscribers is the easiest thing in the world</p>
</div>
<div class="buy-x-how-steps">
    <div class="step">
        <span>01</span>
        <p>Select package</p>
        <img class="icon" alt="Select Package" title="Select Package" src="src/img/buy-followers-how-step-1.svg">
    </div>
    <div class="step">
        <span>02</span>
        <p>Enter username</p>
        <img class="icon" alt="Enter Username" title="Enter Username" src="src/img/buy-followers-how-step-2.svg">
    </div>
    <div class="step">
        <span>03</span>
        <p>Pay order</p>
        <img class="icon" alt="Checkout" title="Checkout" src="src/img/buy-followers-how-step-3.svg" style="width: 75px">
    </div>
</div>
<div class="buy-x-how-expect">
    <h3>We guarantee ..</h3>
    <div class="expect-list">
        <div class="expect-item">
            <img class="icon" alt="Instant Delivery" title="Instant Delivery" src="src/img/buy-followers-expect-3.svg">
            <p>Instant Delivery</p>
        </div>
        <div class="expect-item">
            <img class="icon" alt="Best Quality" title="Best Quality" src="src/img/buy-followers-expect-2.svg">
            <p>Best Quality</p>
        </div>
        <div class="expect-item">
            <img class="icon" alt="Persistent Support" title="Persistent Support" src="src/img/buy-followers-expect-1.svg">
            <p>Persistent Support</p>
        </div>
    </div>
</div>
    </div>
@show
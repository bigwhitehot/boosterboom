@section('why-chooses')
<div id="landing-why">
  	<div id="landing-why-heading">
        <div class="heading">
            <h2>Why choose us</h2>
        </div>
    </div>
    <div id="landing-why-content">
        <div class="section">
            <div class="item">
               <div class="heading">
                  <h3>Daily labor</h3>
               </div>
               <p>We are a team of young developers who literally live in social networks and software development. We are not saying that we are the best. But every day we strive to become them.</p>
            </div>

            <div class="item">
                <div class="heading">
                    <h3>Our technology</h3>
                </div>
                <p>Standard solutions are not for us. We invent our methods and technologies. Therefore, we guarantee the uniqueness of the service.</p>
            </div>
        </div>

        <div class="section">
            <div class="item">
                <div class="heading">
                    <h3>Speed and safety</h3>
                </div>
                <p>We provide services not only quickly, but also safely for your accounts. Thanks to well-functioning technologies, not a single client account was hurt and did not fall under any sanctions.</p>
            </div>

            <div class="item">
                <div class="heading">
                    <h3>Focus on results</h3>
                </div>
                <p>Your result is our priority. Therefore, in case of dissatisfaction with the result, we return the money or compensate for the service.</p>
            </div>
        </div>
    </div>
</div>
@show
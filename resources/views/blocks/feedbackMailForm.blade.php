@section('feedbackMailForm')
<div id="support-page">
    <div id="support-contact">
        <div id="support-contact-header">
            <h1>Write me</h1>
        </div>
        <p id="support-contact-header-subline">Your message will be sent to the ticket system and will reply to it as quickly as possible.</p>
        <form id="support-contact-form" action="mail">
            <div id="support-contact-form-content">
                <div class="form-error" style="display: none;">
                    <div class="icon">
                        <img src="src/img/icon-cross.svg" />
                    </div>
                    <p></p>
                </div>
                <div class="form-input-wrapper half">
                    <label>You instagram nickname</label>
                    <input id="form-name-input" type="text" />
                </div>
                <div class="form-input-wrapper half">
                    <label>E-Mail</label>
                    <input id="form-email-input" type="email" />
                </div>
                <div class="form-input-wrapper">
                    <label>Category</label>
                    <select id="form-subject-select">
                        <option value="question">Other questions</option>
                        <option value="technical_issue">Technical message</option>
                    </select>
                    <img class="icon" src="src/img/icon-angle-cyan.svg" />
                </div>
                <div class="form-input-wrapper">
                    <label>Your message</label>
                    <textarea id="form-message-input"></textarea>
                </div>
                <input type="submit" class="button cyan" value="Send">
            </div>
        </form>
    </div>
</div>

    <div id="hidden-content">
        <div id="contact-sent">
            <img class="icon" src="src/img/icon-checkmark-cyan.svg" />
            <p>Message send</p>
        </div>
        <div class="card-loader">
            <img src="src/img/icon-spinner-cyan.svg" />
            <p class="card-loader-text">Message sending...</p>
        </div>
    </div>

@show
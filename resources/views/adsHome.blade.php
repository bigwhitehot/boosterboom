<!DOCTYPE html>
<html lang="en" class="adsHome-html">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="shortcut icon" href="/src/img/logoB.png" type="image/png">
        <link rel="stylesheet" href="/src/css/adsHome.css">
        <title>BoosterBoom</title>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-165983192-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-165983192-1');
        </script>
    </head>
    <body class="adsHome-body">
        <div class="adsHome-logo">
            <a href="/" title="To Homepage" class="adsHome-logo_all">
                <img src="/src/img/loogo.png" alt="logo" title="logo" width="60px">
                <p class="menu-container-logo-text adsHome-logo_all_text">BoosterBoom</p>
            </a>
        </div>
        <div class="adsHome-basic">
            <div class="adsHome-basic_text">
                Use the services of our agency to promote you on Instagram, for this click on the button.
            </div>
            <div class="adsHome-basic_button">
                <button class="adsHome-basic_button_GetPrices">
                    Get Prices
                </button>
            </div>
        </div>
        <script>
            document.addEventListener('DOMContentLoaded', () => {
                const getPriceButton = document.querySelector('.adsHome-basic_button_GetPrices')
                    .addEventListener('click', () => {
                        window.location = '/{{ $ads }}';
                    });
            });
        </script>
    </body>
</html>
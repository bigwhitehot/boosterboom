@extends('layouts.faq')
@section('content')
<div class="continer container">
    <div class="module-title title">Terms Of Service</div>
    <div class="sub-content">
        <div class="sub-title tgb16">INTRODUCTION</div>
        <div class="des tgr14">
            <span>1</span>. Your use of any services or products (also
            referred to as “offering”) offered by BoosterBoom is subject to
            the terms and conditions mentioned in this document, without
            prejudice to any additional terms which may be part of the
            agreement specific to the service that you wish to avail. Your
            agreement with BoosterBoom will always include, at a minimum, the
            terms and conditions set out in this document.
        </div>
        <div class="des tgr14"><span>2</span>. The terms “BoosterBoom”, “we”, “us”, “our” or any
            grammatical variation of the preceding terms shall refer to
            BoosterBoom , whereas the terms “you”, “your” or any grammatical
            variation shall refer to our clients or the user of any of our
            interfaces including but not restricted to our websites or
            emails.
        </div>
        <div class="des tgr14"><span>3</span>. Acceptance of Terms of Services <br>
            3.1 You are expected to be aware of our terms of services prior
            to making use of our services. Your use of our websites or any
            other means of communication to interact with us shall be
            construed as your acceptance of our Terms of Service even if no
            transaction has taken place. <br>
            3.2 In order to make use of our services you must agree to the
            terms laid down in this document. You may not make use of our
            services if you do not agree to our terms of service.
        </div>
        <div class="des tgr14"><span>4</span>. Provision of the Offering by BoosterBoom <br>
            4.1 BoosterBoom intends to provide you with the finest services
            and products and we would try to innovate our services with
            time. You acknowledge that the nature or the form of the
            services offered by us may change with time. <br>
            4.2 You acknowledge and agree that BoosterBoom may stop
            (permanently or temporarily) providing the offering (or any
            features within the offering) with or without notice, generally
            at BoosterBoom's sole discretion. <br>
            4.3 You acknowledge and agree that while BoosterBoom may not
            currently limit your use of the offering in any way, it may do
            so if your use hinders with the ability of BoosterBoom to carry on
            its operations or the ability of other customers to use the
            offering. <br>
            4.4 The supply of the offering will be subject to your making
            payments at the regular intervals in the manner as stated in the
            contract for the service. In the event of the service being
            permanently or temporarily stopped by us, BoosterBoom's liability
            shall extend only to forfeiting the outstanding amount due for
            the current billing cycle. BoosterBoom shall not be called on to
            repay any amount received by us during any previous billing
            cycle. <br>
            4.5 BoosterBoom shall not be responsible for any loss or damage
            caused by modification of the features, limitation of use of our
            offering or the discontinuation altogether thereof.
        </div>
        <div class="des tgr14"><span>5</span>. Use of BoosterBoom's Offering <br>
            5.1 In order to access certain offering or for your continued
            use of the offering, you may be required to provide information
            about yourself. You agree that any information you give to
            BoosterBoom will always be accurate, correct and up to date.
            BoosterBoom shall not be liable for any loss or damage caused due
            to faulty information provided to us by you <br>
            5.2 We may share client’s personal information with our
            employees or any third parties as and when considered necessary
            by us (e.g. certain information of our clients could be shared
            with third parties for the process of payment for our services).
            For more information regarding the manner in which client’s
            personal information handled by BoosterBoom kindly refer to our
            Privacy Policy. <br>
            5.3 Unless you have been specifically permitted to do so in a
            separate agreement with BoosterBoom, you agree that you will not
            reproduce, duplicate, copy, sell, trade or resell the offering
            for any purpose. <br>
            5.4 You agree that you are solely responsible for any breach of
            obligations on your part of the Terms mentioned in this document
            or in any contract between you and BoosterBoom and for the
            consequences (including any loss or damage which BoosterBoom may
            suffer) of any such breach. <br>
            5.5 We try to ensure that any confidential information provided
            by you is protected. You agree and acknowledge that in the very
            unlikely event of your credentials becoming known to a third
            party on account of an intrusion into BoosterBoom's database,
            BoosterBoom shall not be made liable for the resulting damages.
            <br>5.6 The third party web sites may use technology to send
            (or “serve”) the content of our web site and to process payments
            for the purchase of services on our web site etc. Third parties
            may host the servers that deliver our hosted services to our
            customers. Hyperlinks to the websites of third parties might be
            placed on our websites. The use of the services offered by such
            third parties shall not be subject to this document or our
            Privacy Policy. BoosterBoom provides a platform to help our users
            in making use of services provided by other parties. BoosterBoom
            shall not be responsible for the services provided by any third
            party.
        </div>
        <div class="des tgr14"><span>6</span>. Security of your account <br>
            6.1 BoosterBoom shall provide its services primarily through its
            websites. You might need to make use of a web-based account to
            make use of our services. You agree and understand that you are
            responsible to BoosterBoom and to third parties for maintaining
            the confidentiality of passwords associated with any account you
            use to access the offering. You will be solely responsible for
            all activities that occur under your account. <br>
            6.2 BoosterBoom will take due care to ensure the confidentiality
            of your credentials. You agree and acknowledge that in the very
            unlikely event of your credentials becoming known to a third
            party on account of an intrusion into BoosterBoom database,
            BoosterBoom shall not be made liable for the resulting damages.
            <br>
            6.3 If you become aware of any unauthorized use of your password
            or of your account, you agree to notify the concerned personnel
            at BoosterBoom immediately.
        </div>
        <div class="des tgr14"><span>7</span>. Security of your account <br>
            7.1 BoosterBoom shall provide its services primarily through its
            websites. You might need to make use of a web-based account to
            make use of our services. You agree and understand that you are
            responsible to BoosterBoom and to third parties for maintaining
            the confidentiality of passwords associated with any account you
            use to access the offering. You will be solely responsible for
            all activities that occur under your account. <br>
            7.2 BoosterBoom will take due care to ensure the confidentiality
            of your credentials. You agree and acknowledge that in the very
            unlikely event of your credentials becoming known to a third
            party on account of an intrusion into BoosterBoom database,
            BoosterBoom shall not be made liable for the resulting damages.
            <br>
            7.3 If you become aware of any unauthorized use of your password
            or of your account, you agree to notify the concerned personnel
            at BoosterBoom immediately.
        </div>
        <div class="des tgr14"><span>8</span>. Content in the offering <br>
            8.1 You should be aware that content presented to you as part of
            the offering, including but not limited to advertisements and
            promotional material of BoosterBoom or other companies, is
            protected by intellectual property rights which are owned by
            BoosterBoom, or the sponsors or advertisers who provide that
            content to BoosterBoom (or by other persons or companies on their
            behalf). You may not modify, rent, lease, loan, sell,
            distribute, copy or create derivative work based on this content
            (either in whole or in part) unless you have been specifically
            permitted to do so by BoosterBoom or by the owners of that
            content, in a separate agreement. <br>
            8.2 Any content being disseminated using BoosterBoom's sales
            network or the product, service or platform may be pre-screened,
            reviewed, flagged, filtered, modified or simply refused or
            removed. Any spam or pornographic material and / or any illegal
            content will be immediately deleted and we reserve the right to
            take appropriate legal action. <br>
            8.3 You agree that you are solely responsible for (and that
            BoosterBoom has no responsibility to you or to any third party
            for) any content that you create, transmit or display while
            using our offering or for the consequences of your actions
            (including any loss or damage which BoosterBoom may suffer) by
            doing so. <br>
            8.4 You understand that by using the offering you may be exposed
            to content of other users that you may find offensive, indecent
            or objectionable and that, in this respect, you use the offering
            at your own risk. BoosterBoom shall not be made responsible for
            any repugnant content circulated on its offering by other users.
            On noticing any such content, it is your duty to bring it to the
            attention of BoosterBoom employees immediately.
        </div>
        <div class="des tgr14"><span>9</span>. Intellectual Property Rights <br>
            9.1 You acknowledge and agree that BoosterBoom owns all legal
            right, title and interest in and to the offering, including any
            intellectual property rights which subsist in the offering
            (whether those rights happen to be registered or not, and
            wherever in the world those rights may exist). You further
            acknowledge that the offering may contain information which is
            designated confidential by BoosterBoom and that you shall not
            disclose such information without BoosterBoom's prior written
            consent. Unauthorised use of our trademark, logos etc shall be
            punishable under appropriate the United States laws. <br>
            9.2 You additionally agree that in using the offering, you will
            not use any trade mark, service mark, trade name, logo of any
            company or organization in a way that is likely or intended to
            cause confusion about the owner or authorized user of such
            marks, names or logos. Services obtained from BoosterBoom through
            unauthorized use of such trademarks, service mark etc shall be
            considered as forgery. <br>
            9.3 BoosterBoom acknowledges and agrees that it obtains no right,
            title or interest from you (or your licensors) under these terms
            in or to any content that you submit, post, transmit or display
            on, or through, the offering, including any intellectual
            property rights which subsist in that content (whether those
            rights happen to be registered or not, and wherever in the world
            those rights may exist). Unless you have agreed otherwise in
            writing with BoosterBoom, you agree that you are responsible for
            protecting and enforcing those rights and that BoosterBoom has no
            obligation to do so on your behalf. <br>
            9.4 BoosterBoom gives you a Personal Non-Exclusive license to use
            the interface provided to you by us as part of the offering.
            This license is for the sole purpose of enabling you to use and
            enjoy the benefit of the offering as provided by BoosterBoom, in
            the manner permitted by these terms. <br>
            9.5 The source code of the interface provided by us is protected
            under various laws. You may not (and you may not permit anyone
            else to) copy, modify, create a derivative work of, reverse
            engineer, decompile or otherwise attempt to extract the source
            code of the interface or any part there of. <br>
            9.6 Unless BoosterBoom has given you specific written permission
            to do so, you may not assign (or grant a sub-license of) your
            rights to use the service or otherwise transfer any part of your
            rights to use the service. <br>
            9.7 The payment mode for the offering may be either fixed,
            invoice based or recurring. The payment cycle will be in the
            manner decided at the discretion of BoosterBoom.
        </div>
        <div class="des tgr14"><span>10</span>. Termination of your relationship with BoosterBoom
            <br>
            10.1 The Terms will continue to apply until terminated by either
            you or BoosterBoom as set out below. <br>
            10.2 BoosterBoom may at any time, terminate its legal agreement
            with you if: <br>
            10.2.1 You have breached any provision of the terms (or have
            acted in manner which clearly shows that you do not intend to,
            or are unable to comply with the provisions of the terms); or
            BoosterBoom is required to do so by law (for example, where the
            provision of the offering to you is, or becomes, unlawful).
            <br>
            10.2.2 When these terms come to an end, all of the legal rights,
            obligations and liabilities that you and BoosterBoom are subject
            to in relation to the obligation to maintain confidentiality or
            such other legal rights, obligations and liabilities which are
            expressed to continue indefinitely, shall be unaffected by this
            cessation.
        </div>
        <div class="des tgr14"><span>11</span>. Refund Policy BoosterBoom provide 30-days money
            back guarantee. Charges may be refunded if BoosterBoom is unable
            to provide the services offered and described on this website.
        </div>
    </div>
    <div class="sub-content">
        <div class="sub-title tgb16">EXCLUSION OF WARRANTIES</div>
        <div class="des tgr14"><span>1</span>. YOU EXPRESSLY UNDERSTAND AND AGREE THAT YOUR USE
            OF THE OFFERING IS AT YOUR SOLE RISK AND THAT THE OFFERING ARE
            PROVIDED ON AN “AS IS
        </div>
        <div class="des tgr14"><span>2</span>. IN PARTICULAR, BoosterBoom DOES NOT REPRESENT OR
            WARRANT TO YOU THAT: <br>
            (A) YOUR USE OF THE OFFERING WILL MEET YOUR REQUIREMENTS, <br>
            (B) YOUR USE OF THE OFFERING WILL BE UNINTERRUPTED, TIMELY,
            SECURE OR FREE FROM ERROR <br>
            (C) ANY INFORMATION OBTAINED BY YOU AS A RESULT OF YOUR USE OF
            THE OFFERING WILL BE ACCURATE OR RELIABLE, AND
        </div>
        <div class="des tgr14"><span>3</span>. ANY MATERIAL DOWNLOADED OR OTHERWISE OBTAINED
            THROUGH THE USE OF THE OFFERING IS DONE AT YOUR OWN DISCRETION
            AND RISK AND BoosterBoom WILL NOT BE RESPONSIBLE FOR ANY DAMAGE TO
            YOUR COMPUTER SYSTEM OR OTHER DEVICE OR LOSS OF DATA THAT
            RESULTS FROM THE DOWNLOAD OF ANY SUCH MATERIAL.
        </div>
        <div class="des tgr14"><span>4</span>. NO ADVICE OR INFORMATION, WHETHER ORAL OR
            WRITTEN, OBTAINED BY YOU FROM BoosterBoom OR THROUGH OR FROM THE
            OFFERING SHALL CREATE ANY WARRANTY NOT EXPRESSLY STATED IN THE
            TERMS.
        </div>
        <div class="des tgr14"><span>5</span>. BoosterBoom FURTHER EXPRESSLY DISCLAIMS ALL
            WARRANTIES AND CONDITIONS OF ANY KIND, WHETHER EXPRESS OR
            IMPLIED, INCLUDING, BUT NOT LIMITED TO THE IMPLIED WARRANTIES
            AND CONDITIONS OF TRADE, FITNESS FOR A PARTICULAR PURPOSE AND
            NON-INFRINGEMENT.
        </div>
    </div>
    <div class="sub-content">
        <div class="sub-title tgb16">LIMITATION OF LIABILITIES</div>
        <div class="des tgr14">
            SUBJECT TO OVERALL PROVISION IN CLAUSE 11 ABOVE, YOU EXPRESSLY
            UNDERSTAND AND AGREE THAT BoosterBoom SHALL NOT BE LIABLE TO YOU
            FOR: <br>
            (A) ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, OR CONSEQUENTIAL
            WHICH MAY BE INCURRED BY YOU, HOWEVER CAUSED AND UNDER ANY
            THEORY OF LIABILITY. THIS SHALL INCLUDE, BUT NOT BE LIMITED TO,
            ANY LOSS OF PROFIT (WHETHER INCURRED DIRECTLY OR INDIRECTLY),
            ANY LOSS OF GOODWILL OR BUSINESS REPUTATION, ANY LOSS OF DATA
            SUFFERED, COST OF PROCUREMENT OF SUBSTITUTE GOODS OR OFFERING,
            OR OTHER INTANGIBLE LOSS; <br>
            (B) ANY LOSS OR DAMAGE WHICH MAY BE INCURRED BY YOU, INCLUDING
            BUT NOT LIMITED TO LOSS OR DAMAGE AS A RESULT OF: ANY CHANGES
            WHICH BoosterBoom MAY MAKE TO THE OFFERING, OR FOR ANY PERMANENT
            OR TEMPORARY CESSATION IN THE PROVISION OF THE OFFERING (OR ANY
            FEATURES WITHIN THE OFFERING); <br>
            THE DELETION OF, CORRUPTION OF, OR FAILURE TO STORE, ANY CONTENT
            AND OTHER COMMUNICATIONS DATA MAINTAINED OR TRANSMITTED BY OR
            THROUGH YOUR USE OF THE OFFERING; <br>
            YOUR FAILURE TO PROVIDE BoosterBoom WITH ACCURATE ACCOUNT
            INFORMATION; YOUR FAILURE TO KEEP YOUR PASSWORD OR ACCOUNT
            DETAILS SECURE AND CONFIDENTIAL;
        </div>
    </div>
    <div class="sub-content">
        <div class="sub-title tgb16">Changes to the Terms</div>
        <div class="des tgr14"><span>1</span>. BoosterBoom may make changes to the terms from
            time to time. BoosterBoom shall try to inform you of such changes
            as early as possible. Your continued use of the offering will
            indicate your acceptance of the modified terms.
        </div>
        <div class="des tgr14"><span>2</span>. You understand and agree that if you use the
            offering after the date on which the terms have changed, or
            after the date by which you have to respond as per these terms
            has passed, BoosterBoom will treat your use as acceptance of the
            amended terms.
        </div>
    </div>
    <div class="sub-content">
        <div class="sub-title tgb16">General legal terms</div>
        <div class="des tgr14"><span>1</span>. The terms constitute the legal agreement between
            you and BoosterBoom and govern your use of the offering, but
            without prejudice to any additional terms which may be part of
            an agreement specific to the service you wish to avail of (but
            excluding any offering which BoosterBoom may provide to you under
            a separate written agreement) , and completely replace any prior
            agreements between you and BoosterBoom in relation to the
            offering.
        </div>
        <div class="des tgr14"><span>2</span>. You agree that if BoosterBoom does not exercise or
            enforce any legal right or remedy which is contained in the
            terms (or which BoosterBoom has the benefit of under any
            applicable law), this will not be taken to be a formal waiver of
            BoosterBoom's rights and that those rights or remedies will still
            be available to BoosterBoom.
        </div>
    </div>
    <div class="sub-content">
        <div class="sub-title tgb16">Dispute Resolution</div>
        <div class="des tgr14"><span>1</span>. You and BoosterBoom agree that any dispute
            regarding agreement(s) between you and us shall be referred to
            arbitration. The arbitrator shall be appointed by BoosterBoom and
            the seat of arbitration shall be the United States.
        </div>
        <div class="des tgr14"><span>2</span>. Any other dispute or disagreement of a legal
            nature will also be decided in accordance with the laws of the
            United States, and the Courts of the United States shall have
            exclusive jurisdiction in all such cases.
        </div>
    </div>
</div>
@endsection
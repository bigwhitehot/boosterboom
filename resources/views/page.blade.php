@extends('layouts.app')
@section('description', $page->metaDescription)
@section('keywords', $page->metaKeywords)
@section('title', "BoosterBoom")
@section('content')
@include('items.notification-payment')
   <div id="buy-followers-updated" class="buy-x-v2">
      @include('blocks.top')
      
      @foreach ($page->getListBlocksModels() as $key => $block)
      @include("blocks.{$block->name}", $page->getBlockModelsArrayOptions($block->name))
      @endforeach
      @include('blocks.two-buttons')
   </div>
   <footer>
      <div id="footer-content">
         <div id="footer-sitemap">

            <ul style="justify-content: space-around;display: flex;">
               @foreach ($menu as $key => $menuItem)
                  <a class="menu-item" href="{{$menuItem->link}}" title="{{$menuItem->name}}">
                     <span>{{$menuItem->name}}</span>
                     <div class="menu-animated-underline"></div>
                  </a>
               @endforeach
            </ul>
         </div>
         <div id="footer-sitemap" style="width: 80%;margin-left: auto;margin-right: auto;">
                    <ul style="justify-content: space-around;display: flex;">
                        <li><a href="/terms">Terms of Service</a></li>
                        <li><a href="privacy-policy">Privacy Policy</a></li>
                    </ul>
         </div>
         <div class="footer-divider"></div>
         <div id="footer-signature">
            <p>Copyright © 2020 - <a href="/" class="copyright-name">BoosterBoom.com</a></p>
         </div>
      </div>
   </footer>
@endsection
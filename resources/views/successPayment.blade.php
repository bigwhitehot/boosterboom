@extends('layouts.payment')
   @section('content')
        <div class="payment-wrapper">
           <div class="payment-container">
                   <div class="payment-top">
                       <h1>Thanks for purchase</h1>
                       <div class="order-id-wrapper">Number you order is
                           @if(session()->get('orderID') == null)
                               <div class="order-id">Try now</div>
                           @else
                               <div class="order-id">{{ $orderID }}</div>
                           @endif
                       </div>
                   </div>
                   <div class="payment-button">
                       <input class="button blue" type="button" value="< GO BACK" onclick="goBackButton()">
                   </div>
               <script>
                   function goBackButton() {
                       document.location.href = '/';
                   }
               </script>
           </div>
        </div>

   @endsection

<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" prefix="og: http://ogp.me/ns#">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-title" content="BoosterBoom">
        <meta name="application-name" content="BoosterBoom">
        <meta name="description" content="@yield('description')">
        <meta name="keywords" content="@yield('keywords')">
        <link rel="apple-touch-icon" href="faviconx64.png">
        <link rel="shortcut icon" href="/src/img/logoB.png" type="image/png">
        <link rel="stylesheet" href="/src/css/reset.css">
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">
        @if(isset($page))
            <link rel="stylesheet" href="/src/css/payment.css">
            @if($page->url == "views" ||
                $page->url == "followers" ||
                $page->url == "likes" ||
                $page->url == "comments")
                <link rel="stylesheet" href="/src/css/category.css">
            @endif
            @if($page->url == 'index')
                <link rel="stylesheet" href="/src/css/category.css">
                <link rel="stylesheet" href="/src/css/special-offer.css">
            @endif
            @if($page->url == "special")
                <link rel="stylesheet" href="/src/css/category.css">
                <link rel="stylesheet" href="/src/css/special-offer.css">
            @endif
        @endif
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title')</title>
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-165983192-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-165983192-1');
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <style>@-webkit-keyframes chatra-chat-appear-from-bottom{from{opacity:0;-webkit-transform:translateY(20px) scale(.97);transform:translateY(20px) scale(.97)}to{opacity:1;-webkit-transform:translateY(0) scale(1);transform:translateY(0) scale(1)}}@keyframes chatra-chat-appear-from-bottom{from{opacity:0;-webkit-transform:translateY(20px) scale(.97);transform:translateY(20px) scale(.97)}to{opacity:1;-webkit-transform:translateY(0) scale(1);transform:translateY(0) scale(1)}}@-webkit-keyframes chatra-chat-appear-from-top{from{opacity:0;-webkit-transform:translateY(-20px) scale(.97);transform:translateY(-20px) scale(.97)}to{opacity:1;-webkit-transform:translateY(0) scale(1);transform:translateY(0) scale(1)}}@keyframes chatra-chat-appear-from-top{from{opacity:0;-webkit-transform:translateY(-20px) scale(.97);transform:translateY(-20px) scale(.97)}to{opacity:1;-webkit-transform:translateY(0) scale(1);transform:translateY(0) scale(1)}}@-webkit-keyframes chatra-chat-appear{from{opacity:0;-webkit-transform:scale(.95);transform:scale(.95)}to{opacity:1;-webkit-transform:scale(1);transform:scale(1)}}@keyframes chatra-chat-appear{from{opacity:0;-webkit-transform:scale(.95);transform:scale(.95)}to{opacity:1;-webkit-transform:scale(1);transform:scale(1)}}@-webkit-keyframes chatra-round-button-appear{from{opacity:0;-webkit-transform:scale(.5);transform:scale(.5)}to{opacity:1;-webkit-transform:scale(1);transform:scale(1)}}@keyframes chatra-round-button-appear{from{opacity:0;-webkit-transform:scale(.5);transform:scale(.5)}to{opacity:1;-webkit-transform:scale(1);transform:scale(1)}}@-webkit-keyframes chatra-tab-button-appear{from{opacity:0;-webkit-transform:scale(.9);transform:scale(.9)}to{opacity:1;-webkit-transform:scale(1);transform:scale(1)}}@keyframes chatra-tab-button-appear{from{opacity:0;-webkit-transform:scale(.9);transform:scale(.9)}to{opacity:1;-webkit-transform:scale(1);transform:scale(1)}}@-webkit-keyframes chatra-transparent-appear{from{opacity:0}to{opacity:1}}@keyframes chatra-transparent-appear{from{opacity:0}to{opacity:1}}html.chatra-mobile-widget-expanded,body.chatra-mobile-widget-expanded{overflow:hidden !important;height:100% !important;width:100% !important;position:fixed !important;margin:0 !important;top:0 !important;left:0 !important}#chatra{visibility:hidden;opacity:0;position:fixed;max-height:calc(100% - 40px);max-width:calc(100% - 40px);-webkit-transition:.2s linear;transition:.2s linear;-webkit-transition-property:visibility,opacity;transition-property:visibility,opacity;-webkit-backface-visibility:hidden;backface-visibility:hidden;width:auto;height:auto;min-height:0;min-width:0;display:block;-webkit-box-sizing:content-box;box-sizing:content-box;padding:0;margin:0;}@media print{#chatra{display:none}}#chatra__iframe-wrapper,#chatra__iframe{left:0 !important;top:0 !important;margin:0 !important;padding:0 !important;display:block !important;background:transparent !important}#chatra__iframe-wrapper,#chatra:not(.chatra--toggling) #chatra__iframe,#chatra__iframe.chatra__iframe--mode-frame{height:100% !important;width:100% !important;min-width:100% !important;max-width:100% !important;min-height:100% !important;max-height:100% !important}#chatra__iframe-wrapper{position:absolute;-webkit-box-shadow:0 0 3px rgba(0,0,0,0.1),0 0 10px rgba(0,0,0,0.3);box-shadow:0 0 3px rgba(0,0,0,0.1),0 0 10px rgba(0,0,0,0.3);overflow:hidden !important}#chatra #chatra__iframe-wrapper,#chatra.chatra--webkit #chatra__iframe{border-radius:10px}#chatra.chatra--mobile-widget:not(.chatra--expanded),#chatra.chatra--mobile-widget:not(.chatra--expanded) *{cursor:pointer}#chatra.chatra--webkit #chatra__iframe{-webkit-mask-image:-webkit-gradient(linear,left top, left bottom,from(#000),to(#000));-webkit-mask-image:linear-gradient(#000,#000);mask-image:-webkit-gradient(linear,left top, left bottom,from(#000),to(#000));mask-image:linear-gradient(#000,#000);-webkit-mask-position:50% 50%;mask-position:50% 50%;-webkit-mask-size:100% 100%;mask-size:100% 100%;-webkit-mask-repeat:no-repeat;mask-repeat:no-repeat}#chatra:not(.chatra--expanded) #chatra__iframe-wrapper{-webkit-transition:.2s linear;transition:.2s linear;-webkit-transition-property:-webkit-box-shadow;transition-property:-webkit-box-shadow;transition-property:box-shadow;transition-property:box-shadow, -webkit-box-shadow}#chatra:not(.chatra--expanded):hover #chatra__iframe-wrapper{-webkit-box-shadow:0 0 3px rgba(0,0,0,0.1),0 0 15px rgba(0,0,0,0.4);box-shadow:0 0 3px rgba(0,0,0,0.1),0 0 15px rgba(0,0,0,0.4)}#chatra.chatra--visible{visibility:visible;opacity:1}#chatra.chatra--animating{-webkit-transition:350ms cubic-bezier(.25,.1,0,1);transition:350ms cubic-bezier(.25,.1,0,1);-webkit-transition-property:height,width,max-width,max-height,top,bottom,left,right,-webkit-transform;transition-property:height,width,max-width,max-height,top,bottom,left,right,-webkit-transform;transition-property:height,width,max-width,max-height,top,bottom,left,right,transform;transition-property:height,width,max-width,max-height,top,bottom,left,right,transform,-webkit-transform}#chatra.chatra--style-round:not(.chatra--expanded) #chatra__iframe-wrapper,#chatra.chatra--style-round.chatra--webkit:not(.chatra--expanded) #chatra__iframe{border-radius:50%}#chatra.chatra--fast-toggle.chatra--style-round:not(.chatra--expanded) #chatra__iframe-wrapper{-webkit-animation:chatra-round-button-appear 150ms ease-out 50ms both;animation:chatra-round-button-appear 150ms ease-out 50ms both}#chatra.chatra--fast-toggle.chatra--style-tab:not(.chatra--expanded) #chatra__iframe-wrapper{-webkit-animation:chatra-tab-button-appear 150ms ease-out 50ms both;animation:chatra-tab-button-appear 150ms ease-out 50ms both}#chatra.chatra--fast-toggle.chatra--expanded #chatra__iframe-wrapper{-webkit-animation:chatra-chat-appear 150ms ease-out 50ms both;animation:chatra-chat-appear 150ms ease-out 50ms both}#chatra.chatra--fast-toggle.chatra--transparent #chatra__iframe-wrapper{-webkit-animation-name:chatra-transparent-appear;animation-name:chatra-transparent-appear}#chatra.chatra--fast-toggle.chatra--expanded:not(.chatra--mobile-widget):not(.chatra--transparent) #chatra__iframe-wrapper{-webkit-animation-name:chatra-chat-appear-from-bottom;animation-name:chatra-chat-appear-from-bottom}#chatra.chatra--fast-toggle.chatra--pos-top.chatra--expanded:not(.chatra--mobile-widget):not(.chatra--transparent) #chatra__iframe-wrapper{-webkit-animation-name:chatra-chat-appear-from-top;animation-name:chatra-chat-appear-from-top}#chatra.chatra--expanded #chatra__iframe-wrapper{-webkit-box-shadow:0 0 3px rgba(0,0,0,0.1),0 5px 50px rgba(0,0,0,0.2);box-shadow:0 0 3px rgba(0,0,0,0.1),0 5px 50px rgba(0,0,0,0.2)}#chatra.chatra--side-bottom{bottom:20px}#chatra.chatra--side-left{left:20px}#chatra.chatra--side-left.chatra--style-tab:not(.chatra--expanded){left:10px}#chatra.chatra--side-right{right:20px}#chatra.chatra--side-right.chatra--style-tab:not(.chatra--expanded){right:10px}#chatra.chatra--side-left.chatra--expanded{bottom:20px;left:20px}#chatra.chatra--side-right.chatra--expanded{bottom:20px;right:20px}#chatra.chatra--pos-right{right:20px}#chatra.chatra--pos-left{left:20px}#chatra.chatra--pos-center{left:50%}#chatra.chatra--pos-top.chatra--style-tab:not(.chatra--expanded){bottom:100%;margin-bottom:-20px}#chatra.chatra--pos-top.chatra--style-round:not(.chatra--expanded){top:20px}#chatra.chatra--pos-bottom:not(.chatra--expanded){bottom:20px}#chatra.chatra--pos-middle:not(.chatra--expanded){bottom:50%}#chatra.chatra--mobile-widget.chatra--expanded:not(.chatra--transparent){max-width:none;max-height:none;top:0 !important;bottom:0 !important}#chatra.chatra--mobile-widget.chatra--expanded #chatra__iframe-wrapper,#chatra.chatra--webkit.chatra--mobile-widget.chatra--expanded #chatra__iframe{border-radius:.1px}#chatra.chatra--mobile-widget.chatra--expanded.chatra--pos-right:not(.chatra--transparent){right:0}#chatra.chatra--mobile-widget.chatra--expanded.chatra--pos-left:not(.chatra--transparent){left:0}#chatra.chatra--mobile-widget.chatra--expanded.chatra--pos-center:not(.chatra--transparent){left:0}#chatra.chatra--mobile-widget.chatra--expanded.chatra--side-right:not(.chatra--transparent){right:0}#chatra.chatra--mobile-widget.chatra--expanded.chatra--side-left:not(.chatra--transparent){left:0}#chatra.chatra--transparent.chatra--expanded{bottom:10px;}#chatra.chatra--transparent.chatra--expanded #chatra__iframe-wrapper{-webkit-box-shadow:none;box-shadow:none}#chatra.chatra--side-left.chatra--transparent,#chatra.chatra--pos-left.chatra--transparent{left:10px}#chatra.chatra--side-right.chatra--transparent,#chatra.chatra--pos-right.chatra--transparent{right:10px}#chatra.chatra--mobile-widget.chatra--transparent{max-height:251px}</style>
    </head>
    <body>
        @if(isset($page))
            @yield('content')
            @if($page->url == "faq")
                <script src="/src/js/support.js"></script>
            @endif
            @if($page->url == "views" ||
                $page->url == "followers" ||
                $page->url == "likes" ||
                $page->url == "index" ||
                $page->url == "special" ||
                $page->url == "comments")
                <script src="/src/js/menu.js"></script>
                <script src="/src/js/index.js"></script>
                <script src="{{ mix('js/category.js') }}"></script>
                <script type="text/javascript">
                    $(document).ready(function () {
                        var menu = new Menu().init("#menu-inner", "#menu-mobile-icon");
    
                        $(window).resize(function () {
                            menu.resize();
                        });
    
                        if (document.querySelector("#list-category") == null) {
                            $("#scroll-down-icon").on("click", function () {
                                $("html, body").animate({
                                    scrollTop: document.querySelector("#advantages").offsetTop
                                }, 300);
                                return false;
                            });
                        } else {
                            $("#scroll-down-icon").on("click", function () {
                                $("html, body").animate({
                                    scrollTop: document.querySelector("#list-category").offsetTop
                                }, 300);
                                return false;
                            });
                        };
                    });
                </script>
            @endif
        @else
            <div class="container" id="app">
                @yield('content')
            </div>
            <script src="{{ mix('js/app.js') }}"></script>
            <script src="/src/js/blockui.js"></script>
        @endif

    </body>
</html>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/src/css/payment.css">
    <link rel="stylesheet" href="/src/css/reset.css">
    <link rel="stylesheet" href="/src/css/style.css_id=7d44bac96fc19d8a95f8.css">
    <link rel="stylesheet" href="/src/css/category.css">
    <title>Paypal Payment</title>
</head>
<body>
<menu id="menu-wrapper">
    <div id="menu-content">
        <div class="logo">
            <a href="/" title="To Homepage"><img src="/src/img/site-logo-icon.png" alt="logo" title="logo" width="auto">
                <p class="menu-container-logo-text" style="color: #fff;top: -23px;">BoosterBoom</p>
            </a>
        </div>
        <div id="menu-mobile-icon">
            <div class="dash"></div>
            <div class="dash"></div>
            <div class="dash"></div>
        </div>
        <div id="menu-inner">
            <a class="menu-item" href="../likes" title="Likes">
                <span>Likes</span>
                <div class="menu-animated-underline"></div>
            </a>

            <a class="menu-item" href="../followers" title="Followers">
                <span>Followers</span>
                <div class="menu-animated-underline"></div>
            </a>

            <a class="menu-item" href="../views" title="Views">
                <span>Views</span>
                <div class="menu-animated-underline"></div>
            </a>

            <a class="menu-item" href="../autolikes" title="Autolikes">
                <span>Autolikes</span>
                <div class="menu-animated-underline"></div>
            </a>

            <a class="menu-item" href="../support" title="FAQ">
                <span>FAQ &amp; Support</span>
                <div class="menu-animated-underline"></div>
            </a>
        </div>
    </div>
</menu>
    <div class="container" id="app">
        @yield('content')
    </div>

    <footer>
        <div id="footer-content">
            <div id="footer-sitemap">
                <ul>
                    <li><a href="/likes">Likes</a></li>
                    <li><a href="/views">Views</a></li>
                    <li><a href="/followers">Followers</a></li>
                    <li><a href="/autolikes">Autolikes</a></li>
                </ul>
            </div>
            <div class="footer-divider"></div>
            <div id="footer-signature">
                <p>Copyright © 2020 - <a href="/" class="copyright-name">BoosterBoom.com</a></p>
            </div>
        </div>
    </footer>
</body>
</html>
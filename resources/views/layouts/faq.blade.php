<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" prefix="og: http://ogp.me/ns#">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-title" content="BoosterBoom">
        <meta name="application-name" content="BoosterBoom">
        <meta name="description" content="@yield('description')">
        <meta name="keywords" content="@yield('keywords')">
        <link rel="apple-touch-icon" href="faviconx64.png">
        <link rel="icon" type="image/png" href="faviconx64.png">
        <link rel="stylesheet" href="/src/css/reset.css">
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">
        <link rel="stylesheet" href="/src/css/category.css">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-165983192-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-165983192-1');
        </script>

        <style>

            .container {
                padding-bottom: 40px;
            }
            .sub-title {
                margin-bottom: 32px;
                font-size: 28px;
                line-height: 35px;
                color: unset
            }
            .sub-content {
                max-width: 1000px;
                margin: 30px auto;
                padding-bottom: 30px;
            }
            .sub-content .des {
                font-size: 18px;
                line-height: 25px;
                margin-bottom: 29px;
            }
            
            .des span {
                font-weight: 700;
            }

            .privacy-policy {
                max-width: 1000px;
                margin: 15px auto;
                font-size: 18px;
                padding: 50px 0;
            }

            .privacy-policy p {
                line-height: 25px;
                font-weight: 400;
                font-size: 18px;
            }
            .privacy-policy h1 {
                padding-top: 40px;
                font-weight: 800;
                font-size: 38px;
                margin-bottom: 30px;
                z-index: 2;
                position: relative;
                line-height: 35px;
            }

            .privacy-policy h2,h3 {
                padding: 20px 0;
                display: inline-block;
                vertical-align: middle;
                font-weight: 800;
                font-size: 28px;
                line-height: 35px;
            }
        </style>
   </head>

    <body>
        <menu id="menu-wrapper">
            <div id="menu-content" style="position: relative;    padding-top: 65px;">
               <div class="logo">
                  <a href="/" title="To Homepage" style="margin-top: -16px; "><img src="/src/img/loogo.png" alt="logo" title="logo" width="60px">
                     <p class="menu-container-logo-text" style="color: #fff;top: -20px;margin-left: 5px;font-family: Nunito, Helvetica, Sans-serif;
                        font-size: 20px;    position: relative;display: inline;">BoosterBoom</p>
                  </a>
               </div>
               <div id="menu-mobile-icon" style="margin-top: -54px;">
                  <div class="dash"></div>
                  <div class="dash"></div>
                  <div class="dash"></div>
               </div>
               <div id="menu-inner" style="width: 67%;position: absolute;top: 84px;right: 1%;">
   
                  <a class="menu-item" href="../followers" title="Followers">
                     <span>Followers</span>
                     <div class="menu-animated-underline"></div>
                  </a>
   
                  <a class="menu-item" href="../likes" title="Likes">
                     <span>Likes</span>
                     <div class="menu-animated-underline"></div>
                  </a>
   
                  <a class="menu-item" href="../views" title="Views">
                     <span>Views</span>
                     <div class="menu-animated-underline"></div>
                  </a>
   
   
                  <a class="menu-item" href="../faq" title="Support">
                     <span>Support</span>
                     <div class="menu-animated-underline"></div>
                  </a>
   
               </div>
            </div>
            </div>
          
         </menu>

        @yield('content')

        <footer>
            <div id="footer-content">
                <div id="footer-sitemap">
                    <ul style="justify-content: space-around;display: flex;">
                        <li><a href="/followers">Followers</a></li>
                        <li><a href="/likes">Likes</a></li>
                        <li><a href="/views">Views</a></li>
                        <li><a href="/faq">Support</a></li>
                    </ul>
                </div>
                <div id="footer-sitemap" style="width: 80%;margin-left: auto;margin-right: auto;">
                    <ul style="justify-content: space-around;display: flex;">
                        <li><a href="/terms">Terms of Service</a></li>
                        <li><a href="/privacy-policy">Privacy Policy</a></li>
                    </ul>
                </div>
                <div class="footer-divider"></div>
                <div id="footer-signature">
                    <p>Copyright © 2020 - <a href="/" class="copyright-name">BoosterBoom.com</a></p>
                </div>
            </div>
        </footer>
        <script src="js/app.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script type="text/javascript" src="src/js/menu.js"></script>
        <script type="text/javascript" src="src/js/support.js"></script>
        
        <script type="text/javascript">
            $(document).ready(function(){
                var menu = new Menu().init("#menu-inner", "#menu-mobile-icon"),
                    support_manager = new SupportManager().init();
            
                $(window).resize(function(){
                    menu.resize();
                });
            
                $(".question-head").on("click", function(e){
                    if($(e.currentTarget).parent().hasClass("expanded")){
                        $(e.currentTarget).parent().removeClass("expanded");
                    } else {
                        $(e.currentTarget).parent().addClass("expanded");
                    }
                });

                $(".to-contact").on("click", function(){
                        $("html, body").animate({scrollTop: $("#support-contact").offset().top}, 300);
                    });
            })
        </script>
    </body>
</html>
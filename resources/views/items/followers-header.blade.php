@section('followers-header')
    <div class="buy-x-landing-body">
        <div class="buy-x-landing-content">
            <h1>Get Organic Instagram Followers</h1>
            <p>You have an opportunity to choose between real or bot followers. 
                That depends on your needs!</p>
            <img src="src/img/boostfollowers.png"  class="fotoHeader">
        </div>
        <div class="buy-x-landing-illustration">
            @for($i = 1; $i < 12; $i++)
                <img id="plus-particle-{{ $i }}" class="plus-particle" src="src/img/user-plus-solid.svg">
            @endfor
            <div id="illustration-followers-list" class="illustration-part">
                <img class="illustration-followers-list-img" title="Best followers for instagram" src="src/img/header-real-followers-vs.png">
                <h3 class="illustration-followers-list-h3">Real followers</h3>
            </div>

            <div id="illustration-followers-chart" class="illustration-part">
                <h3 class="illustration-followers-chart-h3">Spam bots</h3>
                <img class="illustration-followers-chart-img" alt="Grow Your Account" title="Grow Your Account" src="src/img/header-spambots-vs.png">
            </div>

            <div id="illustration-followers-dot" class="illustration-part">
                <img class="illustration-followers-dot-img" alt="Add Followers" title="Add Followers" src="/src/img/header-vsImg.png">
            </div>

        </div>
    </div>
@show
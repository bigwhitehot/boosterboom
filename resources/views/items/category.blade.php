@section('itemCategory')
    @foreach ($categories as $key => $category)
        @if ($category->toMain == 1)
            <div class="b" style="transform: scale(1,1);">
                <div class="d">
                    <div class="c">
                        @if (!empty($category->isFeatures))
                            <span class="yamuk"><span>{{$category->isFeatures}}</span></span>
                        @endif
                        <div>{{$category->name}}<br><span>{{$category->description}}</span></div>
                    </div>
                    <div class="e">
                        <div class="l">
                            <span class="f">$</span>
                            <span class="int" style="color : #0380ff">
                                {{$category->getLeftPointCost()}}
                            </span><span class="g" style="color : #0380ff">
                                {{$category->getRightPointCost()}}
                            </span>
                        </div>
                    </div>
                    <div>
                        <ul class="h">
                            @foreach ($category->getListAdvantagesAsArray() as $advantage)
                                <li class="i">{{$advantage}}</li>
                            @endforeach
                        </ul>
                    </div>

                    <div class="k">
                        <a href="{{$category->linkOrder}}" class="j buy-button " style="background : #0380ff" id="1000IGF" > Get </a>
                    </div>
                </div>
            </div>
        @endif
    @endforeach
@show
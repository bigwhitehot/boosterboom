@section('likes-header')
    <div class="buy-x-landing-body">
        <div class="buy-x-landing-content">
            <h1>Get instagram likes</h1>
            <p>You can also choose if you want to get likes from real people or bot accounts</p>
            <img src="src/img/boostlikes.png"  class="fotoHeaderLikes">
        </div>
        <div class="buy-x-landing-illustration">
            @for($i = 1; $i < 12; $i++)
                <img id="plus-particle-{{ $i }}" class="plus-particle" src="src/img/heart-solid.svg">
            @endfor
            <img src="src/img/boosterV.png"  class="fotoHeaderLikesBig">
        </div>
    </div>
@show
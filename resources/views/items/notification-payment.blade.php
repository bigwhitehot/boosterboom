@if ($message = Session::get('payment') == 'success')
<div class="alert alert-success" role="alert">
    <p>Thank's for payment. Check your Email</p>
    <span onclick="this.parentElement.style.display='none'" class="closebtn">&times;</span> 
</div>
<?php Session::forget('payment');?>
@endif
@if ($message = Session::get('payment') == 'error')
    <div class="alert alert-error">
        <p>Payment error. If you paid, please, go to Support</p>
        <span onclick="this.parentElement.style.display='none'" class="closebtn">&times;</span> 
    </div>
    <?php Session::forget('payment');?>
@endif
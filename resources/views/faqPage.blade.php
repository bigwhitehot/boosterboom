@extends('layouts.faq')

@section('content')
      <div id="support-page">
         <div id="support-faq" style="background-image: url('img/background_v.svg')">
            <div id="support-header">
               <h1>FAQ</h1>
            </div>
            <p id="support-header-subline">
               Here you will find answers to frequently asked questions about the work of our service. If among them there is no answer to your question, 
               write to us by mail <a class="text-link">boosterboomservice@gmail.com</a>.
            </p>
            <div id="support-faq-questions">
               <div class="question">
                  <div class="question-head">
                     <img class="icon" src="src/img/icon-angle-cyan.svg" />
                     <h3>How long do subscribers follow?</h3>
                  </div>
                  <div class="question-body">
                     <p>
                        The speed doesn’t depend on the tariff you have chosen. The average starting time is 2 hour that is really quick in current realities. Our smart algorithm allows circumventing the Instagram protection. It comes to 2-2,5k followers a day!
                     </p>
                  </div>
               </div>
               <div class="question">
                  <div class="question-head">
                     <img class="icon" src="src/img/icon-angle-cyan.svg" />
                     <h3>How long does it take to add likes?</h3>
                  </div>
                  <div class="question-body">
                     <p>Likes are also added in accordance with our rules. The service considers Instagram protection features and optimizes the process of natural adding. </p>
                  </div>
               </div>
               <div class="question">
                  <div class="question-head">
                     <img class="icon" src="src/img/icon-angle-cyan.svg" />
                     <h3>How long are views added?</h3>
                  </div>
                  <div class="question-body">
                     <p>
                        Orders are sent to processing immediately after payment, but Instagram can update the number of views with a delay of up to several hours.                     </p>
                  </div>
               </div>
               <div class="question">
                  <div class="question-head">
                     <img class="icon" src="src/img/icon-angle-cyan.svg" />
                     <h3>Why buy likes and followers?</h3>
                  </div>
                  <div class="question-body">
                     <p>
                        This is one of the proven and effective way to promote your profile on Instagram. Adding followers and likes helps to attract even more attention and activity from other users.                     </p>
                  </div>
               </div>
               <div class="question">
                  <div class="question-head">
                     <img class="icon" src="src/img/icon-angle-cyan.svg" />
                     <h3>Account not banned?</h3>
                  </div>
                  <div class="question-body">
                     <p>A compulsory condition is having your Instagram account opened. It shouldn’t be closed or blocked to allow followers view and follow it. In case of breach of the terms, the program can give a failure during the execution of services. It means that the services won’t be rendered.</p>
                  </div>
               </div>
               <div class="question">
                  <div class="question-head">
                     <img class="icon" src="src/img/icon-angle-cyan.svg" />
                     <h3>Can I order more subscribers and likes?</h3>
                  </div>
                  <div class="question-body">
                     <p>Yes. To discuss the cost, write to our support. We can also give you a dealer’s discount. </p>
                  </div>
               </div>
               <div class="question">
                  <div class="question-head">
                     <img class="icon" src="src/img/icon-angle-cyan.svg" />
                     <h3>Why should I choose you?</h3>
                  </div>
                  <div class="question-body">
                     <p>
                        We have been on Instagram since 2020. Over the past time, they have made a result for more than 10,000 people and developed their own technologies. We also provide some of the lowest prices for such quality on the market.                     </p>
                  </div>
               </div>
               
            </div>
         </div>
         {{-- <div id="support-contact">
            <div id="support-contact-header">
               <h1>Write to us</h1>
            </div>
            <p id="support-contact-header-subline">
               Your message will be sent to the ticket system and the answer will be received as quickly as possible.
            </p>
            <form id="support-contact-form">
               <div id="support-contact-form-content">
                  <div class="form-error" style="display: none;">
                     <div class="icon">
                        <img src="src/img/icon-cross.svg" />
                     </div>
                     <p></p>
                  </div>
                  <div class="form-input-wrapper half">
                     <label>You instagram nickname</label>
                     <input id="form-name-input" type="text" />
                  </div>
                  <div class="form-input-wrapper half">
                     <label>E-Mail</label>
                     <input id="form-email-input" type="email" />
                  </div>
                  <div class="form-input-wrapper">
                     <label>Category</label>
                     <select id="form-subject-select">
                        <option value="question">Other questions</option>
                        <option value="technical_issue">Tecnical message</option>
                     </select>
                     <img class="icon" src="src/img/icon-angle-cyan.svg" />
                  </div>
                  <div class="form-input-wrapper">
                     <label>Your message</label>
                     <textarea id="form-message-input"></textarea>
                  </div>
                  <input type="submit" class="button cyan" value="Send">
               </div>
            </form>
         </div>
      </div>
      <div id="hidden-content">
         <div id="contact-sent">
            <img class="icon" src="src/img/icon-checkmark-cyan.svg" />
            <p>Message send</p>
         </div>
         <div class="card-loader">
            <img src="src/img/icon-spinner-cyan.svg" />
            <p class="card-loader-text">Message sending ...</p>
         </div>
      </div> --}}
@endsection
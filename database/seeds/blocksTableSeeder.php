<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class blocksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('blocks')->insert($this->dataArray());
    }
    
    final private function dataArray()
    {
        return [
            [
                "name" => "category", 
                "title" => "Категории",
                "description" => "Блок категорий"
            ],
            [
                "name" => "our-uniques",
                "title" => "Our benefits",
                "description" => "Unique block"
            ],
            [
                "name" => "how-it-work",
                "title" => "How does it work",
                "description" => "How Block"
            ],
            [
                "name" => "reviews", 
                "title" => "Отзывы",
                "description" => "Блок отзывов"
            ],
            [
                "name" => "why-choose", 
                "title" => "Why Us",
                "description" => "client side"
            ],
        ];
    }
}

<?php

use Illuminate\Database\Seeder;

class smmPanelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('smm_panels')->insert($this->dataArray());
    }

    final private function dataArray() {
        return [
            [
                'name' => 'justanotherpanel.com',
                'api_key' => 'c164637e89695ddf34defa56bc2ead27',
                'api_url' => 'https://justanotherpanel.com/api/v2',
                'isActive' => 1,
                'email' => 'boosterboomservice@gmail.com',
                'account_name' => 'boosterboomservice',
                'password' => 'password',
            ],
        ];
    }
}
<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class reviewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('reviews')->insert($this->dataArray());
    }
    
    final private function dataArray()
    {
        return [
            [
                "name" => "Isabella", 
                "text" => 'I raised the number of subscribers to my account from 500 thousand to a million. Several thousand unsubscribed, but after the message in support I made a "refill", now everything is fine. I will buy more!',
                "stars" => "4.5",
                "on_off" => "1"
            ],
            [
                "name" => "Diego", 
                "text" => "I regularly buy subscribers in small portions, 100-1000 each. Some unsubscribe, but add already with this in mind. Often there is even more than ordered, thanks!",
                "stars" => "5.0",
                "on_off" => "1"
            ],
            [
                "name" => "Margaret", 
                "text" => "I buy BoosterBoom subscribers for about five months. As a rule, everything comes quickly, but recently subscribers have not added at all. And this is the second time! After the message in support, everything was added, of course, they threw some more on top, but the sediment remained. Hopefully we will do without incident in the future!",
                "stars" => "4.0",
                "on_off" => "1"
            ],
            [
                "name" => "Lauren", 
                "text" => "After a year of working together, I can say: there is simply no better service! I couldn’t find a suitable one for a long time: the likes wouldn’t come, the subscribers would unsubscribe, and sometimes it’s impossible to wait for a response from the support. Everything is different here! The troubles, even if they did arise, were resolved in a matter of minutes by the support workers. We will continue to work together!",
                "stars" => "5.0",
                "on_off" => "1"
            ],
            [
                "name" => "Lilly", 
                "text" => "Once again, acquired 50 thousand subscribers. Came within two days, do not unsubscribe. Thanks guys, the price-quality ratio is really the best on the market!",
                "stars" => "5.0",
                "on_off" => "1"
            ],
        ];
    }
}

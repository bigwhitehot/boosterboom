<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class adminRolePermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_role_permissions')->insert($this->dataArray());
    }
    
    final private function dataArray()
    {
        return [
            [
                "role_id" => 1,
                "permission_id" => 1,
            ]
        ];
    }
}

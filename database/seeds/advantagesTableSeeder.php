<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class advantagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('advantages')->insert($this->dataArray());
    }
    
    final private function dataArray()
    {
        return [
            [
                "name" => 'High Quality Followers', 
                "helpText" => ""
            ],
            [
                "name" => 'All Real & Active', 
                "helpText" => ""
            ],
            [
                "name" => 'Drop Protection', 
                "helpText" => "If you lose followers that you have already purchased, our system will automatically refill the drops for 30 days."
            ],
            [
                "name" => '100% Safe', 
                "helpText" => ""
            ],
            [
                "name" => 'No Password Required', 
                "helpText" => ""
            ],
            [
                "name" => 'Instant Delivery', 
                "helpText" => ""
            ],
            [
                "name" => '24/7 Support', 
                "helpText" => ""
            ],
        ];
    }
}

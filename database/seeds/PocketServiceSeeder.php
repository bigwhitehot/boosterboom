<?php

use Illuminate\Database\Seeder;

class PocketServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pocket_services')->insert($this->dataArray());
    }

    final private function dataArray()
    {
        return [
            [
                'pocket_id' => 1,
                'service_id' => 1,
                'refil' => 30,
                'country' => null,
                'description' => 'followers'
            ]
        ];
    }
}

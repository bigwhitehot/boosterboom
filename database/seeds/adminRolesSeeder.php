<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class adminRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_roles')->insert($this->dataArray());
    }
    
    final private function dataArray()
    {
        return [
            [
                "name" => "Administrator",
                "slug" => "administrator",
            ]
        ];
    }
}

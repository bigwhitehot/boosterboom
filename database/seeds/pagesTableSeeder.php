<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class pagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert($this->dataArray());
    }
    
    final private function dataArray()
    {
        return [
            [
                "url" => "index",
                "title" => "BoosterBoom",
                "metaDescription" => "indexDescription",
                "metaKeywords" => "indexKeywords",
                "listBlocks" => '["1","2","3","4","5"]',
                "content" => ""
            ],
            [
                "url" => "views",
                "title" => "Views",
                "metaDescription" => "ViewsDescription",
                "metaKeywords" => "ViewsKeywords",
                "listBlocks" => '["1", "2"]',
                "content" => ""
            ],
            [
                "url" => 'likes', 
                "title" => "Likes",
                "metaDescription" => "LikesDescription",
                "metaKeywords" => "LikesKeywords",
                "listBlocks" => '["1", "5"]',
                "content" => ""
            ],
            [
                "url" => "followers", 
                "title" => "Followers",
                "metaDescription" => "followersDescription",
                "metaKeywords" => "followersKeywords",
                "listBlocks" => '["1", "2"]',
                "content" => ""
            ],
            [
                "url" => "autolikes",
                "title" => "autolikes",
                "metaDescription" => "autolikesDescription",
                "metaKeywords" => "autolikesKeywords",
                "listBlocks" => '["1", "2"]',
                "content" => ""
            ],
            [
                "url" => "special",
                "title" => "Limited Offers",
                "metaDescription" => "Limited OffersDescription",
                "metaKeywords" => "Limited OffersKeywords",
                "listBlocks" => '["1", "2"]',
                "content" => ""
            ],
        ];
    }
}

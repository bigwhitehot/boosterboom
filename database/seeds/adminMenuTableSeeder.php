<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class adminMenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_menu')->insert($this->dataArray());
    }
    
    final private function dataArray()
    {
        return [
            ["parent_id" => 0, "order" => 1, "title" => "Index", "icon" => "fa-bar-chart", "uri" => "/"],
            ["parent_id" => 0, "order" => 2, "title" => "Admin", "icon" => "fa-tasks", "uri" => ""],
            ["parent_id" => 2, "order" => 3, "title" => "Users", "icon" => "fa-users", "uri" => "auth/users"],
            ["parent_id" => 2, "order" => 4, "title" => "Roles", "icon" => "fa-user", "uri" => "auth/roles"],
            ["parent_id" => 2, "order" => 5, "title" => "Permission", "icon" => "fa-ban", "uri" => "auth/permissions"],
            ["parent_id" => 2, "order" => 6, "title" => "Menu", "icon" => "fa-bars", "uri" => "auth/menu"],
            ["parent_id" => 2, "order" => 7, "title" => "Operation log", "icon" => "fa-history", "uri" => "auth/logs"],
            ["parent_id" => 0, "order" => 8, "title" => "Helpers", "icon" => "fa-gears", "uri" => ""],
            ["parent_id" => 8, "order" => 9, "title" => "Scaffold", "icon" => "fa-keyboard-o", "uri" => "helpers/scaffold"],
            ["parent_id" => 8, "order" => 10, "title" => "Database terminal", "icon" => "fa-database", "uri" => "helpers/terminal/database"],
            ["parent_id" => 8, "order" => 11, "title" => "Laravel artisan", "icon" => "fa-terminal", "uri" => "helpers/terminal/artisan"],
            ["parent_id" => 8, "order" => 12, "title" => "Routes", "icon" => "fa-list-alt", "uri" => "helpers/routes"],

            ["parent_id" => 0, "order" => 13, "title" => "Site", "icon" => "fa-bars", "uri" => "/list-types-category/index"],
            ["parent_id" => 13, "order" => 14, "title" => "Категории", "icon" => "fa-bars", "uri" => "/category/index"],
            ["parent_id" => 13, "order" => 15, "title" => "Преимущества", "icon" => "fa-bars", "uri" => "/advantage/index"],
            ["parent_id" => 13, "order" => 16, "title" => "Отзывы", "icon" => "fa-bars", "uri" => "/reviews/index"],
            ["parent_id" => 13, "order" => 17, "title" => "Страницы", "icon" => "fa-gears", "uri" => "/page/index"],
            ["parent_id" => 13, "order" => 18, "title" => "Menu", "icon" => "fa-gears", "uri" => "/menu/index"],
            ["parent_id" => 13, "order" => 19, "title" => "Blocks", "icon" => "fa-gears", "uri" => "/block/index"],
            ["parent_id" => 0, "order" => 20, "title" => "CashOut", "icon" => "fa-gears", "uri" => "/cash"],
            ["parent_id" => 0, "order" => 21, "title" => "Services", "icon" => "fa-gears", "uri" => "/services"],
            ["parent_id" => 0, "order" => 22, "title" => "PayPal-Cashout", "icon" => "fa-gears", "uri" => "paypal-cashout"],
            ["parent_id" => 0, "order" => 23, "title" => "SMM Panels", "icon" => "fa-gears", "uri" => "smmpanel"],
            ["parent_id" => 0, "order" => 24, "title" => "Proxies", "icon" => "fa-product-hunt", "uri" => "proxies"],
            ["parent_id" => 0, "order" => 25, "title" => "Pocket-services", "icon" => "fa-table", "uri" => "pocket-services"],
            ["parent_id" => 0, "order" => 25, "title" => "Сервисы", "icon" => "fa-table", "uri" => "smm-services"],
            
        ];
    }
}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class adminRoleMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_role_menu')->insert($this->dataArray());
    }
    
    final private function dataArray()
    {
        return [
            [
                "role_id" => 1,
                "menu_id" => 2,
            ]
        ];
    }
}

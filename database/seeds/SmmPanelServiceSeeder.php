<?php

use Illuminate\Database\Seeder;

class SmmPanelServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('smm_services')->insert($this->dataArray());
    }

    final private function dataArray() {
        return [
            [
                'type' => 'followers',
                'smm_panel_id' => 1,
                'service_id' => 1,
                'rate' => 3.4
            ],
        ];
    }
}

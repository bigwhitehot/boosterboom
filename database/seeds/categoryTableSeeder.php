<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class categoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category')->insert($this->dataArray());
    }
    
    final private function dataArray(): Array
    {
        return [
                // Followers
            [
                "name" => '200', 
                "description" => "Bot Followers",
                "count" => 200,
                "special_offer_id" => 1,
                "type" => 'followers',
                "cost" => "2.25",
                "listAdvantages" => '["1","2","3","4","5","6","7"]',
                "linkOrder" => "/checkout/followers/200",
                "toMain" => 1,
                "isFeatures" => ""
            ],
            [
                "name" => '150',
                "description" => "Real Followers",
                "special_offer_id" => 2,
                "count" => 150,
                "type" => 'followers',
                "cost" => "4.25",
                "listAdvantages" => '["1","2","3","4","5","6","7"]',
                "linkOrder" => "/checkout/followers/150",
                "toMain" => 1,
                "isFeatures" => ""
            ],
            [
                "name" => '450', 
                "description" => "Bot Followers",
                "special_offer_id" => 1,
                "count" => 450,
                "type" => 'followers',
                "cost" => "4.45",
                "listAdvantages" => '["1","2","3","4","5","6","7"]',
                "linkOrder" => "/checkout/followers/450",
                "toMain" => 1,
                "isFeatures" => ""
            ],
            [
                "name" => '350', 
                "description" => "Real Followers",
                "special_offer_id" => 2,
                "count" => 350,
                "type" => 'followers',
                "cost" => "8.65",
                "listAdvantages" => '["1","2","3","4","5","6","7"]',
                "linkOrder" => "/checkout/followers/350",
                "toMain" => 1,
                "isFeatures" => ""
            ],
            [
                "name" => '700', 
                "description" => "Bot Followers",
                "special_offer_id" => 3,
                "count" => 700,
                "type" => 'followers',
                "cost" => "5.29",
                "listAdvantages" => '["1","2","3","4","5","6","7"]',
                "linkOrder" => "/checkout/followers/700",
                "toMain" => 1,
                "isFeatures" => "25"
            ],
            [
                "name" => '600', 
                "description" => "Real Followers",
                "special_offer_id" => 4,
                "count" => 600,
                "type" => 'followers',
                "cost" => "11.29",
                "listAdvantages" => '["1","2","3","4","5","6","7"]',
                "linkOrder" => "/checkout/followers/600",
                "toMain" => 1,
                "isFeatures" => "25"
            ],
            [
                "name" => '1200', 
                "description" => "Bot Followers",
                "special_offer_id" => 3,
                "count" => 1200,
                "type" => 'followers',
                "cost" => "10.35",
                "listAdvantages" => '["1","2","3","4","5","6","7"]',
                "linkOrder" => "/checkout/followers/1200",
                "toMain" => 1,
                "isFeatures" => ""
            ],
            [
                "name" => '1000', 
                "description" => "Real Followers",
                "special_offer_id" => 6,
                "count" => 1000,
                "type" => 'followers',
                "cost" => "21.99",
                "listAdvantages" => '["1","2","3","4","5","6","7"]',
                "linkOrder" => "/checkout/followers/1000",
                "toMain" => 1,
                "isFeatures" => ""
            ],
            [
                "name" => '2000', 
                "description" => "Bot Followers",
                "special_offer_id" => 5,
                "count" => 2000,
                "type" => 'followers',
                "cost" => "13.99",
                "listAdvantages" => '["1","2","3","4","5","6","7"]',
                "linkOrder" => "/checkout/followers/2000",
                "toMain" => 1,
                "isFeatures" => "35"
            ],
            [
                "name" => '1500', 
                "description" => "Real Followers",
                "special_offer_id" => 6,
                "count" => 1500,
                "type" => 'followers',
                "cost" => "25.99",
                "listAdvantages" => '["1","2","3","4","5","6","7"]',
                "linkOrder" => "/checkout/followers/1500",
                "toMain" => 1,
                "isFeatures" => "35"
            ],
            [
                "name" => '75', 
                "description" => "Test Followers",
                "special_offer_id" => 7,
                "count" => 75,
                "type" => 'followers',
                "cost" => "0.99",
                "listAdvantages" => '["3","4","5","6","7"]',
                "linkOrder" => "/checkout/followers/75",
                "toMain" => 1,
                "isFeatures" => "70"
            ],
                // LIKES
            [
                "name" => '150', 
                "description" => "Real likes",
                "special_offer_id" => '8',
                "count" => 150,
                "type" => 'likes',
                "cost" => "2.69",
                "listAdvantages" => '["3","4","5","6","7"]',
                "linkOrder" => "/checkout/likes/150",
                "toMain" => 1,
                "isFeatures" => ""
            ],
            [
                "name" => '200', 
                "description" => "Bot likes",
                "count" => 200,
                "special_offer_id" => '9',
                "type" => 'likes',
                "cost" => "1.69",
                "listAdvantages" => '["3","4","5","6","7"]',
                "linkOrder" => "/checkout/likes/200",
                "toMain" => 1,
                "isFeatures" => ""
            ],
            [
                "name" => '350',
                "description" => "Real likes",
                "count" => 350,
                "special_offer_id" => '8',
                "type" => 'likes',
                "cost" => "5.35",
                "listAdvantages" => '["3","4","5","6","7"]',
                "linkOrder" => "/checkout/likes/350",
                "toMain" => 1,
                "isFeatures" => ""
            ],
            [
                "name" => '450', 
                "description" => "Bot likes",
                "count" => 450,
                "special_offer_id" => '9',
                "type" => 'likes',
                "cost" => "2.99",
                "listAdvantages" => '["3","4","5","6","7"]',
                "linkOrder" => "/checkout/likes/450",
                "toMain" => 1,
                "isFeatures" => ""
            ],
            [
                "name" => '600', 
                "description" => "Real likes",
                "count" => 600,
                "special_offer_id" => '10',
                "type" => 'likes',
                "cost" => "7.25",
                "listAdvantages" => '["3","4","5","6","7"]',
                "linkOrder" => "/checkout/likes/600",
                "toMain" => 1,
                "isFeatures" => "25"
            ],
            [
                "name" => '700', 
                "description" => "Bot likes",
                "count" => 700,
                "special_offer_id" => '11',
                "type" => 'likes',
                "cost" => "3.69",
                "listAdvantages" => '["3","4","5","6","7"]',
                "linkOrder" => "/checkout/likes/700",
                "toMain" => 1,
                "isFeatures" => "25"
            ],
            [
                "name" => '1000', 
                "description" => "Real likes",
                "count" => 1000,
                "special_offer_id" => '12',
                "type" => 'likes',
                "cost" => "13.49",
                "listAdvantages" => '["3","4","5","6","7"]',
                "linkOrder" => "/checkout/likes/1000",
                "toMain" => 1,
                "isFeatures" => ""
            ],
            [
                "name" => '1200', 
                "description" => "Bot likes",
                "count" => 1200,
                "special_offer_id" => '13',
                "type" => 'likes',
                "cost" => "7.15",
                "listAdvantages" => '["3","4","5","6","7"]',
                "linkOrder" => "/checkout/likes/1200",
                "toMain" => 1,
                "isFeatures" => ""
            ],
            [
                "name" => '1500', 
                "description" => "Real likes",
                "count" => 1500,
                "special_offer_id" => '12',
                "type" => 'likes',
                "cost" => "16.99",
                "listAdvantages" => '["3","4","5","6","7"]',
                "linkOrder" => "/checkout/likes/1500",
                "toMain" => 1,
                "isFeatures" => "35"
            ],
            [
                "name" => '2000', 
                "description" => "Bot likes",
                "count" => 2000,
                "special_offer_id" => '13',
                "type" => 'likes',
                "cost" => "9.99",
                "listAdvantages" => '["3","4","5","6","7"]',
                "linkOrder" => "/checkout/likes/2000",
                "toMain" => 1,
                "isFeatures" => "35"
            ],
            [
                "name" => '100', 
                "description" => "Test likes",
                "count" => 100,
                "special_offer_id" => '14',
                "type" => 'likes',
                "cost" => "0.99",
                "listAdvantages" => '["3","4","5","6","7"]',
                "linkOrder" => "/checkout/likes/100",
                "toMain" => 1,
                "isFeatures" => "80"
            ],
                // Views
            [
                "name" => '3000', 
                "description" => "Views",
                "count" => 3000,
                "special_offer_id" => '15',
                "type" => 'views',
                "cost" => "4.89",
                "listAdvantages" => '["3","4","5","6","7"]',
                "linkOrder" => "/checkout/views/3000",
                "toMain" => 1,
                "isFeatures" => ""
            ],
            [
                "name" => '5000', 
                "description" => "Views",
                "count" => 5000,
                "special_offer_id" => '16',
                "type" => 'views',
                "cost" => "6.99",
                "listAdvantages" => '["3","4","5","6","7"]',
                "linkOrder" => "/checkout/views/5000",
                "toMain" => 1,
                "isFeatures" => ""
            ],

            [
                "name" => '10000', 
                "description" => "Views",
                "count" => 10000,
                "special_offer_id" => '17',
                "type" => 'views',
                "cost" => "10.75",
                "listAdvantages" => '["3","4","5","6","7"]',
                "linkOrder" => "/checkout/views/10000",
                "toMain" => 1,
                "isFeatures" => "25"
            ],
            [
                "name" => '15000', 
                "description" => "Views",
                "count" => 15000,
                "special_offer_id" => '18',
                "type" => 'views',
                "cost" => "18.25",
                "listAdvantages" => '["3","4","5","6","7"]',
                "linkOrder" => "/checkout/views/15000",
                "toMain" => 1,
                "isFeatures" => ""
            ],
            [
                "name" => '25000', 
                "description" => "Views",
                "count" => 25000,
                "special_offer_id" => '18',
                "type" => 'views',
                "cost" => "24.99",
                "listAdvantages" => '["3","4","5","6","7"]',
                "linkOrder" => "/checkout/views/25000",
                "toMain" => 1,
                "isFeatures" => "35"
            ],
            [
                "name" => '1000', 
                "description" => "Test Views",
                "count" => 1000,
                "special_offer_id" => '19',
                "type" => 'views',
                "cost" => "0.99",
                "listAdvantages" => '["3","4","5","6","7"]',
                "linkOrder" => "/checkout/views/1000",
                "toMain" => 1,
                "isFeatures" => "70"
            ],

                //Special
            [
                "name" => '500', 
                "description" => "Real followers",
                "count" => 500,
                "special_offer_id" => '20',
                "type" => 'special',
                "cost" => "9.99",
                "listAdvantages" => '["3","4","5","6","7"]',
                "linkOrder" => "/checkout/special/500",
                "toMain" => 1,
                "isFeatures" => ""
            ],
            [
                "name" => '900', 
                "description" => "Real likes",
                "count" => 900,
                "special_offer_id" => '21',
                "type" => 'special',
                "cost" => "10.99",
                "listAdvantages" => '["3","4","5","6","7"]',
                "linkOrder" => "/checkout/special/900",
                "toMain" => 1,
                "isFeatures" => ""
            ],
            [
                "name" => '7000', 
                "description" => "Views",
                "count" => 7000,
                "special_offer_id" => '22',
                "type" => 'special',
                "cost" => "6.99",
                "listAdvantages" => '["3","4","5","6","7"]',
                "linkOrder" => "/checkout/special/7000",
                "toMain" => 1,
                "isFeatures" => ""
            ],
        ];
    }
}

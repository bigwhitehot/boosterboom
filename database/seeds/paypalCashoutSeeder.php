<?php

use Illuminate\Database\Seeder;

class paypalCashoutSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('paypal_cashout')->insert($this->dataArray());
    }
    
    final private function dataArray()
    {
        return [
            [
                'secret' => 'EBm-QWVOFcAGrWmKu2RQW_fbX88WxXJS9fZ0PrSupG-sS1ZMhuLkwjZfWsVVIOC1Lmp5SCqeEdHiJCV7',
                'client_id' => 'AYAjWPQACUKzBKSEsLYZe7_40B7SOQp2Ww3k1bWwxG64-1Cwot929uNNqr72_70TICaU4HwqSl0ifz8E',
                'account' => 'negromakov@gmail.com',
                'identity' => 'firstname, lastname and etc info about user',
                'isActive' => 1
            ],
        ];
    }
}
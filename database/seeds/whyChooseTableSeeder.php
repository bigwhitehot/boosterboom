<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class whyChooseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('why_choose')->insert($this->dataArray());
    }
    
    final private function dataArray()
    {
        return [
            [
                "name" => "Daily labor",
                "description" => 'We are a team of young developers who literally live in social networks and software development. We are not saying that we are the best. But every day we strive to become them.'
            ],
            [
                "name" => "Our technology",
                "description" => 'Standard solutions are not for us. We invent our methods and technologies. Therefore, we guarantee the uniqueness of the service.'
            ],
            [
                "name" => "Speed ​​and safety",
                "description" => 'We provide services not only quickly, but also safely for your accounts. Thanks to well-functioning technologies, not a single client account was hurt and did not fall under any sanctions.'
            ],
            [
                "name" => "Focus on results",
                "description" => 'Your result is our priority. Therefore, in case of dissatisfaction with the result, we return the money or compensate for the service.'
            ],
        ];
    }
}

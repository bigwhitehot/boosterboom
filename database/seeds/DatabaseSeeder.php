<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
    	    adminMenuTableSeeder::class,
    	    adminPermissionsTableSeeder::class,
    	    adminRoleMenuSeeder::class,
    	    adminRolePermissionsSeeder::class,
    	    adminRolesSeeder::class,
    	    adminRoleUsersSeeder::class,
			adminUsersTableSeeder::class,
			advantagesTableSeeder::class,
    	    blocksTableSeeder::class,
    	    categoryTableSeeder::class,
			menuTableSeeder::class,
    	    pagesTableSeeder::class,
			smmPanelSeeder::class,
            reviewsTableSeeder::class,
			whyChooseTableSeeder::class,
			paypalCashoutSeeder::class,
			userOrderRequestSeeder::class,
			SpecialOffer::class,
			ProxyTableSeeder::class,
			PocketServiceSeeder::class,
			SmmPanelServiceSeeder::class,
	    ]);
    }
}

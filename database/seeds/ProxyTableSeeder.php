<?php

use Illuminate\Database\Seeder;

class ProxyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('proxies')->insert($this->dataArray());
    }

    final private function dataArray()
    {
        return [
            [
                'url' => 'http://remontom.online/',
                'key' => 'null',
                'isActive' => 1,
                'count' => 0,
            ]
        ];
    }
}

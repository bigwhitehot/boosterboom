<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class adminRoleUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_role_users')->insert($this->dataArray());
    }
    
    final private function dataArray()
    {
        return [
            [
                "role_id" => 1,
                "user_id" => 1,
            ]
        ];
    }
}

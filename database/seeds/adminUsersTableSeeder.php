<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class adminUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_users')->insert($this->dataArray());
    }
    
    final private function dataArray()
    {
        return [
            [
                "username" => "admin", 
                "password" => '$2y$10$4wN/EKXzWeWnNofQgfwpGebcJs6nyPyT0Y7.YRkmBsS3fMj7V8hlG', 
                "name" => "Administrator"
            ],
        ];
    }
}

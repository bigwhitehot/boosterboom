<?php

use Illuminate\Database\Seeder;

class SpecialOffer extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('special_offers')->insert($this->dataArray());
    }

    final private function dataArray()
    {
        return [
            //1
            [
                'type' => 'followers',
                'amount' => 200,
                'cost' => 2.25,
                'total' => 1.55,
            ],
            //2
            [
                'type' => 'followers',
                'amount' => 150,
                'cost' => 4.25,
                'total' => 2.59,
            ],
            //3
            [
                'type' => 'followers',
                'amount' => 450,
                'cost' => 4.45,
                'total' => 3.59,
            ],
            //4
            [
                'type' => 'followers',
                'amount' => 350,
                'cost' => 8.65,
                'total' => 4.99,
            ],
            //5
            [
                'type' => 'followers',
                'amount' => 1000,
                'cost' => 9.99,
                'total' => 7.55,
            ],
            //6
            [
                'type' => 'followers',
                'amount' => 1000,
                'cost' => 21.99,
                'total' => 14.99,
            ],
            // for 1$ pocket

            //7
            [
                'type' => 'followers',
                'amount' => 50,
                'cost' => 0.99,
                'total' => 0.69,
            ],
            

                //likes
                //8
            [
                'type' => 'likes',
                'amount' => 150,
                'cost' => 2.69,
                'total' => 1.89,
            ],
            //9
            [
                'type' => 'likes',
                'amount' => 200,
                'cost' => 1.69,
                'total' => 1.39,
            ],
            //10
            [
                'type' => 'likes',
                'amount' => 350,
                'cost' => 5.35,
                'total' => 4.79,
            ],
            //11
            [
                'type' => 'likes',
                'amount' => 400,
                'cost' => 3.29,
                'total' => 2.69,
            ],
            //12
            [
                'type' => 'likes',
                'amount' => 600,
                'cost' => 7.25,
                'total' => 5.99,
            ],
            //13
            [
                'type' => 'likes',
                'amount' => 1000,
                'cost' => 5.89,
                'total' => 3.99,
            ],
            // for 1$ likes
            //14
            [
                'type' => 'likes',
                'amount' => 100,
                'cost' => 0.99,
                'total' => 0.69,
            ],
            
            //Views
            //15
            [
                'type' => 'views',
                'amount' => 3000,
                'cost' => 4.89,
                'total' => 3.55,
            ],
            //16
            [
                'type' => 'views',
                'amount' => 3000,
                'cost' => 4.89,
                'total' => 3.55,
            ],
            //17
            [
                'type' => 'views',
                'amount' => 5000,
                'cost' => 6.99,
                'total' => 4.49,
            ],
            //18
            [
                'type' => 'views',
                'amount' => 10000,
                'cost' => 10.75,
                'total' => 7.99,
            ],
            // for 1$ pocket views
            //19
            [
                'type' => 'views',
                'amount' => 1000,
                'cost' => 0.99,
                'total' => 0.69,
            ],
            

                //specials
                //20
            [
                'type' => 'special',
                'amount' => 350,
                'cost' => 8.65,
                'total' => 4.99,
            ],
            //21
            [
                'type' => 'special',
                'amount' => 350,
                'cost' => 5.35,
                'total' => 4.79,
            ],

            //22
            [
                'type' => 'special',
                'amount' => 3000,
                'cost' => 4.89,
                'total' => 3.55,
            ]
        ];
    }
}

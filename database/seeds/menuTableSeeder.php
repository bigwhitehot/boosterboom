<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class menuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menu')->insert($this->dataArray());
    }
    
    final private function dataArray()
    {
        return [
            [
                "status" => 1, 
                "sort" => 0,
                "name" => "Followers",
                "link" => "/followers"
            ],
            [
                "status" => 1,
                "sort" => 0,
                "name" => "Likes",
                "link" => "/likes"
            ],
            [
                "status" => 1,
                "sort" => 0,
                "name" => "Views",
                "link" => "/views"
            ],
            [
                "status" => 1, 
                "sort" => 0,
                "name" => "Support",
                "link" => "/faq"
            ],
        ];
    }
}

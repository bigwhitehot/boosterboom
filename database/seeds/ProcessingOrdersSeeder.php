<?php

use Illuminate\Database\Seeder;

class ProcessingOrdersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('processing_orders')->insert($this->dataArray());
    }

    final private function dataArray(){
        return [
            [

            ],
        ];
    }
}

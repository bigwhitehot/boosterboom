<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmmPanelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('smm_panels', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->text('api_key');
            $table->text('api_url');
            $table->text('account_name');
            $table->text('email');
            $table->text('password');
            $table->integer('isActive');
            $table->float('balance')->default(0)->nullable();
            $table->date('updated_at')->nullable();
            $table->date('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('smm_panels');
    }
}

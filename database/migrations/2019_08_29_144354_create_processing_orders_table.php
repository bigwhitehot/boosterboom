<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProcessingOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('processing_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->text('panel', 20)->nullable();
            $table->text('pppid', 100)->nullable();
            $table->text('link', 100)->nullable();
            $table->text('charge', 10)->nullable();
            $table->text('status', 20)->nullable();
            $table->text('email', 50)->nullable();
            $table->integer('order_id');
            $table->integer('start_count')->nullable();
            $table->integer('quantity')->nullable();
            $table->integer('service')->nullable();
            $table->integer('remains')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('processing_orders');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->comment('Название');
            $table->text('description')->comment('Описание');
            $table->integer('special_offer_id')->nullable();
            $table->string('type')->comment('Тип');
            $table->integer('count')->comment('Количество');
            $table->float('cost')->comment('Цена');
            $table->string('listAdvantages')->comment('список преимуществ');
            $table->string('linkOrder')->comment('ссылка на покупку');
            $table->integer('toMain')->default(0)->comment('на главной');
            $table->string('isFeatures')->nullable()->comment('Красная лента');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category');
    }
}

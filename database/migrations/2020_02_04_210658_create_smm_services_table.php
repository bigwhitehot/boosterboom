<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmmServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('smm_services', function (Blueprint $table) {
            $table->increments('id');
            $table->text('type');
            $table->integer('smm_panel_id');
            $table->integer('service_id');
            $table->float('rate')->description('rate per 1000');
            $table->integer('min_quantity')->default(10);
            $table->integer('max_quantity')->default(3000);
            $table->integer('start_time')->nullable();
            $table->integer('speed_per_day')->nullable();
            $table->text('refill')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('smm_services');
    }
}

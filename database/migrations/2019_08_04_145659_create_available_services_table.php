<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvailableServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('available_services', function (Blueprint $table) {
            $table->increments('id');
            $table->string('panel', 55);
            $table->string('name', 255);
            $table->string('category', 255);
            $table->integer('service_id');
            $table->float('cost');
            $table->integer('metrica');
            $table->integer('start_time');
            $table->integer('speed_per_day');
            $table->boolean('refil');
            $table->integer('min');
            $table->integer('max');
            $table->text('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('available_services');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExecuteOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('execute_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('smm_panel_id');
            $table->integer('smm_service_id');
            $table->string('smm_order_id');
            $table->integer('user_order_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('execute_orders');
    }
}

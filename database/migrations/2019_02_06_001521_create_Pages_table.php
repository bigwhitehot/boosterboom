<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url')->comment('Адрес страницы');
            $table->string('title')->comment('Заголовок');
            $table->string('metaDescription')->nullable()->comment('Мета описание');
            $table->string('metaKeywords')->nullable()->comment('Мета ключевые слова');
            $table->text('listBlocks')->nullable();
            $table->text('content')->nullable()->comment('Контент');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}

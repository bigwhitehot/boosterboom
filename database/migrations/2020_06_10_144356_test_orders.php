<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TestOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('test_orders', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('uid');
            $table->boolean('followers')->nullable();
            $table->boolean('views')->nullable();
            $table->boolean('likes')->nullable();
            $table->boolean('autolikes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExist('test_orders');
    }
}

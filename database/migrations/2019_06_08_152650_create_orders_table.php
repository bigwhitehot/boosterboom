<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('user_id');
            $table->text('email')->nullable();
            $table->text('category_id')->nullable();
            $table->text('link')->nullable();
            $table->text('payer_id')->nullable();
            $table->text('token')->nullable();
            $table->text('paypal_payment_id')->nullable();
            $table->float('cost')->unsigned()->nullable();
            $table->integer('payment_status')->unsigned()->default(0);
            $table->string('nickname', 255);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}

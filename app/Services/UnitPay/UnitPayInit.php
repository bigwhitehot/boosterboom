<?php
namespace app\Services\UnitPay;

use UnitPay;

class UnitPayInit
{
    private $domain = 'unitpay.money';
    private $secretKey = '24c915bad87385b6c9b1ee663ae54a57';
    private $publicId = '256551-a9e3c';
    private $projectId = '256551';
    private $orderCurrency = 'USD';
    

    public function __contruct()
    {
        
    }

    public function getUnitPay()
    {
        return new UnitPay($this->domain, $this->secretKey);
    }
}
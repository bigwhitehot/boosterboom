<?php

namespace app\Services\UnitPay;

class OrderData
{
    public $orderId;
    public $orderSum;
    public $orderDesc;
    public $orderCurrency;

    public function __construct($orderId, $orderSum, $orderDesc, $orderCurrency)
    {
        $this->orderId = $orderId;
        $this->orderSum = $orderSum;
        $this->orderDesc = $orderDesc;
        $this->orderCurrency = $orderCurrency;
    }
}
<?php

namespace App\Services\OrderExecute;

use App\Models\SmmOrder;
use App\Models\SmmService;
use App\Models\SmmPanel;
use App\Models\PocketService;

use App\Services\SmmPanelAPI;

class FollowersOrder extends OrderExecute
{
    protected $link;
    protected $SMMPanel;
    protected $SMMService;
    protected $SMMOrder;
    protected $SMMAPI;
    protected $amount;
    
    const FOLLOWERS_LINK = 'https://www.instagram.com/';

    public function executeOrder($order)
    {
        $username = $this->order->username;
        
        $this->amount = $this->getAmount($order);

        $this->link = self::FOLLOWERS_LINK . $username . '/';
        SmmPanel::refreshBalances();
         
        $this->SMMPanel = $this->getSmmPanels($order)[0]; // убрать нули работает толькро для 1 панели и для 1 сервиса
        $this->SMMAPI = new SmmPanelAPI($this->SMMPanel);
        // $this->SMMService = $this->getSmmService($this->SMMPanel)[0]; // убрать нули работает толькро для 1 панели и для 1 сервиса
        $services = PocketService::getServicesCollectionById($this->order->pocket->id);
        if ($services[0]) {
            $this->SMMService = $services[0]->service;
        } else {
            return responce('No available service' . self, 500);
        }

        // if ($this->order->additional_data != 'false') {
        //     $additionalData = json_decode($this->order->additional_data);
        //     $refill = $additionalData->refill;
        //     $country = $additionalData->country;
        //     if ($country != 'random' || $country != '') {
        //         foreach ($services as $service) {
        //             if ($service->country == $country) {
        //                 $this->SMMService = $service->service;
        //             break;
        //             }
        //         }
        //     } else {
        //         foreach ($services as $service) {
        //             if ($service->country == $country) {
        //                 $this->SMMService = $service->service;
        //             break;
        //             }
        //         }
        //     }
        //     if (!$this->SMMService) {
        //         return response('No Available service ' . self::class, 500);
        //     }
        //     dd('4', $this);
        // }
        // на время тестирования сделал рандомный номер ордера с панели

        $SmmOrderNumber = $this->SMMAPI->order(array(
            'service' => $this->SMMService->service_id,
            'link' => $this->link,
            'quantity' => $this->amount
        ));

        $this->SMMOrder = SmmOrder::create([
            'order_id' => $this->order->id,
            'smm_order_id' => $SmmOrderNumber->order,
            'smm_panel_id' => $this->SMMPanel->id,
            'smm_service_id' => $this->SMMService->id
        ]);
    }
}
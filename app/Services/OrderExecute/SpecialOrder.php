<?php

namespace App\Services\OrderExecute;

use App\Models\SmmOrder;
use App\Models\SmmService;
use App\Models\SmmPanel;
use App\Models\PocketService;

use App\Services\SmmPanelAPI;

class SpecialOrder extends OrderExecute
{
    protected $SMMAPI;
    protected $SMMPanel;
    protected $SMMService;
    protected $amount;
    protected $SMMOrder;
    protected $orderLinks;
    protected $smmOrders = [];

    private $baseLink = 'https://www.instagram.com/p/';

    public function executeOrder($order)
    {
        SmmPanel::refreshBalances();

        $description = $order->pocket->description;
        $pocket = $order->pocket;

        $this->amount = $this->getAmount($order);
        $this->orderLinks = json_decode($this->order->links);

        $this->SMMPanel = $this->getSmmPanels($order)[0];
        $this->SMMAPI = new SmmPanelAPI($this->SMMPanel);

        $services = PocketService::getServicesCollectionById($this->order->pocket->id);
        if ($services[0]) {
            $this->SMMService = $services[0]->service;
        } else {
            return responce('No available service' . self, 500);
        }

        if (preg_match('/views/i', $description)) {
            $this->executeViews();
            return $this;
        } elseif (preg_match('/likes/i', $description)) {
            $this->executeLikes($pocket);
            return $this;
        } elseif (preg_match('/followers/i', $description)) {
            $this->executeFollowers($pocket);
            return $this;
        }
    }

    private function executeViews()
    {
        foreach ($this->orderLinks as $link) {
            $SmmOrderNumber = rand(100, 9999);
            // $SmmOrderNumber = $this->SMMAPI->order(array(
            //     'service' => $this->SMMService->service_id,
            //     'link' => $this->baseLink . $link . '/',
            //     'quantity' => 10 //$this->amount // временно сделал 10
            //  ));
            $this->createSmmOrder($SmmOrderNumber);
            array_push($this->smmOrders, $SmmOrderNumber);
        }

        $this->smmOrders = json_encode($this->smmOrders);
        
        return;
    }

    private function executeLikes()
    {
        foreach ($this->orderLinks as $link) {
            $SmmOrderNumber = rand(100, 9999);
            // $SmmOrderNumber = $this->SMMAPI->order(array(
            //     'service' => $this->SMMService->service_id,
            //     'link' => $this->baseLink . $link . '/',
            //     'quantity' => 10 //$this->amount // временно сделал 10
            //  ));
            $this->createSmmOrder($SmmOrderNumber);
            array_push($this->smmOrders, $SmmOrderNumber);
        }

        $this->smmOrders = json_encode($this->smmOrders);
        
        return;
    }

    private function executeFollowers()
    {
        $order = new FollowersOrder($this->order);
        return $order;
    }

    private function createSmmOrder($SmmOrderNumber)
    {
        $this->SMMOrder = SmmOrder::create([
            'order_id' => $this->order->id,
            'smm_order_id' => $SmmOrderNumber->order,
            'smm_panel_id' => $this->SMMPanel->id,
            'smm_service_id' => $this->SMMService->id
        ]);

        return;
    }
}
<?php

namespace App\Services\OrderExecute;

use App\Models\SmmPanel;
use App\Models\UserOrder;
use App\Models\CategoryModel;
use App\Models\ExecuteOrder;
use App\Models\PocketService;

use App\Services\SmmPanelAPI;

class OrderExecute implements IOrderExecute
{
    protected $order;
    protected $pocket;
    public $executedOrder;

    public function __construct(UserOrder $order)
    {
        $this->pocket = $order->pocket;
        $this->order = $order;
        $this->executeOrder($order);
    }

    public function executeOrder($order)
    {
        switch ($this->pocket->type) {
            case 'followers' : $this->executedOrder = new FollowersOrder($order); break;
            case 'likes' : $this->executedOrder = new LikeOrder($order); break;
            case 'views' : $this->executedOrder = new  LikeOrder($order); break;
            case 'special' : {
                $description = $this->order->pocket->description;
                if (preg_match('/views/i', $description)) {
                    $this->executedOrder = new LikeOrder($order); 
                } elseif (preg_match('/likes/i', $description)) {
                    $this->executedOrder = new LikeOrder($order);
                } elseif (preg_match('/followers/i', $description)) {
                    $this->executedOrder = new FollowersOrder($order);
                }
            break;
            }
            default;
        }
        foreach ($this->executedOrder as $value => $key) {
            if (!$key) {
                return response('Error' . $value . 'undefined');
            }
        }
        $this->createInternalOrder($this->executedOrder);
    }

    public function getServices($panels) {
        
    }

    protected function getAmount($order)
    {
        if ($order->special_offer != 'false') {
            $pocketCount = $order->pocket->count;
            $specialOfferCount = $order->pocket->specialOffer->amount;
            return $pocketCount + $specialOfferCount;
        } else {
            return $order->pocket->count;
        }
    }

    protected function createInternalOrder($executedOrder)
    {
        $options = array(
            'internal_id' => 0,
            'smm_panel_id' => $executedOrder->SMMPanel->id,
            'smm_service_id' => $executedOrder->SMMService->id,
            'smm_order_id' => $executedOrder->SMMOrder->id,
            'user_order_id' => $this->order->id
        );
        ExecuteOrder::create($options);
    }
    
    protected function getSmmPanels($order)
    {
        $additionalData = json_decode($order->additional_data);
        $SMMPanels = SMMPanel::getActiveSmmPanels();
        if ($SMMPanels) {
            return $SMMPanels;
        } else {
            dd('No have active Panels');
        }
    }

    protected function getSmmService($panel)
    {

        //temporarely make table for pocket and service


        $panelService = [];
        $services = $panel->getServicesByType($this->pocket->type);
        return $services;
    }

    // Выбор нужного сервиса с панели
    protected function getAdditionalDataServices($services, $additionalData)
    {
        $target = $additionalData->country;
        $refill = $additionalData->refill;
        $additionalDataServices = [];
        foreach ($services as $service => $keys) {
            foreach ($keys as $key) {
                if ($key->refill == 'true') {
                    array_push($additionalDataServices, $key);
                }
            }
        }
        return $additionalDataServices;
    }

    private function getRelevantService($services)
    {
        //temporarily!!! fix this later
        var_dump('Change later', self::class);
        dd($this->order);
        return $services[0];
    }

    public function getAmountPerPost($amount, $arrayLength)
    {
        return (int)round($amount / $arrayLength);
    }
}
<?php

namespace App\Services\OrderExecute;


interface IOrderExecute
{
    public function executeOrder(UserOrder $order);
    public function getServices($panels);
}
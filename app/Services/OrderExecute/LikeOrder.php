<?php

namespace App\Services\OrderExecute;

use App\Models\SmmOrder;
use App\Models\SmmService;
use App\Models\SmmPanel;
use App\Models\PocketService;

use App\Services\SmmPanelAPI;

class LikeOrder extends OrderExecute
{
    protected $SMMPanel;
    protected $SMMService;
    protected $SMMOrder;
    protected $SMMAPI;
    protected $amount;
    protected $orderLinks;
    protected $smmOrders = [];

    private $baseLink = 'https://www.instagram.com/p/';

    public function executeOrder($order)
    {
        $this->amount = $this->getAmount($order);
        $this->orderLinks = json_decode($this->order->links);
        $amountPerPost = $this->getAmountPerPost($this->amount, count($this->orderLinks));
        
        SmmPanel::refreshBalances();
        
        $this->SMMPanel = $this->getSmmPanels($order)[0]; // убрать нули работает толькро для 1 панели и для 1 сервиса
        $this->SMMAPI = new SmmPanelAPI($this->SMMPanel);
        // $this->SMMService = $this->getSmmService($this->SMMPanel)[0]; // убрать нули работает толькро для 1 панели и для 1 сервиса
        // времмено написан выбор сервиса с помощью таблицы соответствия pocket_services
        $services = PocketService::getServicesCollectionById($this->order->pocket->id);
        if ($services[0]) {
            $this->SMMService = $services[0]->service;
        } else {
            return responce('No available service' . self, 500);
        }

        // на время тестирования сделал рандомный номер ордера с панели
        
        foreach ($this->orderLinks as $link) {
            $SmmOrderNumber = $this->SMMAPI->order(array(
                'service' => $this->SMMService->service_id,
                'link' => $this->baseLink . $link . '/',
                'quantity' => $amountPerPost
             ));
            $this->SMMOrder = SmmOrder::create([
                'order_id' => $this->order->id,
                'smm_order_id' => $SmmOrderNumber->order,
                'smm_panel_id' => $this->SMMPanel->id,
                'smm_service_id' => $this->SMMService->id
            ]);

            array_push($this->smmOrders, $SmmOrderNumber);
        }

        $this->smmOrders = json_encode($this->smmOrders);
        return $this;
    }
}
<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Mail extends Mailable
{
    protected $order;

    public function __construct($order)
    {
        $this->order = $order;
    }

    public function build()
    {
        return $this->from('boosterboomservice@gmail.com')
            ->subject('BoosterBoom.com : Order information')
            ->view('mail.message')
            ->with([    
                'orderId' => $this->order->userTransaction->uid
            ]);
    }
}
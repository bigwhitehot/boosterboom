<?php
namespace App\Http\Controllers;

use UnitPay;

use App\Models\UserTransaction;

class UnitPayController extends Controller
{

    public function testPay()
    {
        return view('vendor/unitpay/payCard', ['body' => $body]);
    }


    /**
     * Search the order if the request from unitpay is received.
     * Return the order with required details for the unitpay request verification.
     *
     * @param Request $request
     * @param $order_id
     * @return mixed
     */
    public static function searchOrderFilter(Request $request, $order_id) {

        // If the order with the unique order ID exists in the database
        $transaction = UserTransaction::where('token', $order_id)->first();

        if ($transaction) {
            $order = $transaction->userOrder;
            $order['UNITPAY_orderSum'] = $order->amount; // from your database
            $order['UNITPAY_orderCurrency'] = 'RUB';  // from your database

            // if the current_order is already paid in your database, return strict "paid"; 
            // if not, return something else
            $order['UNITPAY_orderStatus'] = $order->order_status; // from your database
            return $order;
        }

        return false;
    }

    /**
     * When the payment of the order is received from unitpay, you can process the paid order.
     * !Important: don't forget to set the order status as "paid" in your database.
     *
     * @param Request $request
     * @param $order
     * @return bool
     */
    public static function paidOrderFilter(Request $request, $order)
    {
        // Your code should be here:
        YourOrderController::saveOrderAsPaid($order);

        // Return TRUE if the order is saved as "paid" in the database or FALSE if some error occurs.
        // If you return FALSE, then you can repeat the failed paid requests on the unitpay website manually.
        return true;
    }

    /**
     * Process the request from the UnitPay route.
     * searchOrderFilter is called to search the order.
     * If the order is paid for the first time, paidOrderFilter is called to set the order status.
     * If searchOrderFilter returns the "paid" order status, then paidOrderFilter will not be called.
     *
     * @param Request $request
     * @return mixed
     */
    public function payOrderFromGate(Request $request)
    {
        return UnitPay::payOrderFromGate($request);
    }
}
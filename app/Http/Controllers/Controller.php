<?php

namespace App\Http\Controllers;

use App\Jobs\UpdateProcesingOrders;
use App\Models\AdvantageModel;
use Illuminate\Routing\Controller as BaseController;
use App\Models\CategoryModel;
use App\Models\PageModel;
use App\Models\ReviewsModel;
use Illuminate\Http\Request;
use App\Models\WhyChooseModel;
use App\Models\MenuModel;
use App\Models\SmmPanelModel;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail as SwiftMailer;

use Illuminate\Support\Facades\Redirect;

use App\Models\UsersToken;
use App\Models\UserTransaction;
use App\Models\UserOrder;
use App\Models\SmmPanel;
use App\Models\SpecialOffer;
use App\Models\PocketService;
use App\Models\Proxy;

use App\Mail\Mail;

use App\Services\OrderExecute\OrderExecute;

use App\Services\UnitPay\UnitPayInit;
use UnitPay;

class Controller extends BaseController
{
    protected const PROXY_URI = '/proxy/validate?token=';
    
    public function welcome(Request $request)
    {
        if ($request->input('payment') == 'abort') {
            $request->session()->put('payment', 'abort');
            $token = $request->input('token');
            $transaction = UserTransaction::getLatestTransactionByToken($token);
            $transaction->setIsAbort(1);
            $transaction->setTransactionStatus(4);
            $transaction->userOrder->update(['payed' => 'abort']);
            return redirect('/?userPayment=abort');
        }
        return view('page')->with([
            "categories" => CategoryModel::where('type', '=', 'special')->get(),
            "reviews" => ReviewsModel::all(),
            "page" => PageModel::where('url', "index")->firstOrFail(),
            "whyChooses" => WhyChooseModel::all(),
            "menu" => MenuModel::all(),
            "advantages" => AdvantageModel::getAdvantagesNameList()->toJson()
        ]);
    }

    public function adsHome(Request $request)
    {
        $ads = $request->input('ads');
        $pages = ['likes', 'followers', 'special', 'views'];
        if(in_array($ads, $pages)) {
            return view('adsHome', ['ads' => $ads]);
        } else {
            return view('adsHome', ['ads' => '']);
        }
    }

    public function orderConstructor($type = null, $count = null, Request $request)
    {
        if (!is_null($type)) {
            $categories = CategoryModel::where("type", $type)->get()->toJson();
            $specialOffer = SpecialOffer::getPocketByType($type)->toJson();
        } else {
            abort(404);
        }

        return view('blocks.pay', [
            "categories" => $categories,
            "specialOffer" => $specialOffer,
            "type" => $type,
            "count" => $count
        ]);
    }
    
    public function pay(Request $request){
        return view('blocks/pay');
    }

    public function test(Request $request)
    {
        $payment_amount = '1';

        $payment_no = '1';

        $user_email = 'galaginc@gmail.com';

        $item_name = '1 item';

        $currency = 'USD';

        // return view('vendor/unitpay/payment_form', ['payment_fields' => $paymentFields]);
        // UnitPay::payOrderFromGate($request);
        return UnitPay::generatePaymentForm($payment_amount, $payment_no, $user_email, $item_name, $currency);
    }

    public function proxy(Request $request) {

        // $proxy = Proxy::getActiveProxy();
        // $proxyURL = $proxy->url . self::PROXY_URI;

        $token = $request->session()->get('_token');
        $session = $request->session()->getId();
        $pocketId = $request->pocket_id;
        $pocket = CategoryModel::find($pocketId);
        
        //create user offer
        $offer = UserOrder::create($request->all());

        //create user transaction
        $uid = md5($token . $session);
        $offer->userTransaction()->create([
            'token' => $token,
            'session_id' => $session,
            'uid' => $uid,
        ]);

        $transaction = $offer->userTransaction;
        if ($offer->special_offer == 'true') {
            $specialOfferAmount = $offer->pocket->specialOffer->amount;
        } else $specialOfferAmount = 0;

        $totalAmount = $specialOfferAmount + $offer->pocket->count;

        $account = $transaction->uid;
        $currency = 'USD';
        $desc =  $totalAmount . ' ' . $offer->pocket->description;
        $sum = $this->getPriceByTransaction($offer->userTransaction);
        $secretKey = config('unitpay.UNITPAY_SECRET_KEY');

        $hashStr = $account.'{up}'.$currency.'{up}'.$desc.'{up}'.$sum.'{up}'.$secretKey;

        $signature = hash('sha256', $hashStr);

        $data = [
            'sum' => $sum,
            'account' => $account,
            'desc' => $desc,
            'currency' => $currency,
            'locale' => 'en',
            'signature' => $signature,
            'hideOtherMethods' => 'false'
        ];

        $unitPayUrl = 'https://unitpay.money/pay/256551-a9e3c/card';
        // $curl = curl_init($unitPayUrl);
        // curl_setopt_array($curl, array(
        //     CURLOPT_POST => true,
        //     CURLOPT_SSL_VERIFYPEER => false,
        //     CURLOPT_SSL_VERIFYHOST => false,
        //     CURLOPT_POSTFIELDS => $data,
        //     CURLOPT_RETURNTRANSFER => true,
        //     CURLOPT_HEADER => false,
        //     CURLOPT_AUTOREFERER => true,
        //     CURLOPT_FOLLOWLOCATION => true,
        // ));
        // $response = curl_exec($curl);
        // curl_close($curl);

        $url = $unitPayUrl . '?' .http_build_query($data);

        // //redirect user to proxy with $uid trought browser methods
        return response($url);
        
    }

    public function paymentCancel(Request $request)
    {
        session()->put('payment', 'error');
        $token = $request->input('account');
        $transaction = UserTransaction::getLatestTransactionByToken($token);
        $transaction->setTransactionStatus(3);
        $order = $transaction->userOrder;
        $order->update(array(
            'payed' => 'cancel'
        ));
        return redirect('/');
    }

    public function paymentSuccess(Request $request)
    {
        $params = $request->input();
        $token = $params['account'];
        $paymentStatus = 1;
        
        $transaction = UserTransaction::getLatestTransactionByToken($token);
        if ($transaction->transaction_status == 2) { 
            return response('Don\'t abuse');
        } else {
            $transaction->setTransactionStatus(1);
        }

        if ($transaction) {
            if ($transaction->transaction_status == 1) {
                $transaction->setTransactionStatus(2);
                $order = $transaction->userOrder;
                if ($order) {
    
                    $options = array(
                        'payed' => 'payed',
                    );
                    $order->update($options);
    
                    //execute order, send user order to SMM panel
                    $execute = new OrderExecute($order);
    
                    //return to main page with parameter success
    
                    SwiftMailer::to($order->email)
                        ->send(new Mail($order));

                    session()->put('payment', 'success');
                    return redirect('/');
    
                } else {
                    return response('Order not found, go to Support. 
                        You transaction ID'
                        . $token
                    );
                }
            } else {
                return response('Transaction not found, go to Support. 
                    You transaction ID: '
                    . $token
                );
            }
        } else {
            abort(404, "Not valid transaction");
        }
    }

    private function getPriceByTransaction($transaction)
    {
        $offer = $transaction->userOrder;

        $additionalDataPrice;
        $specialOfferPrice;

        $pocketPrice = $offer->pocket->cost;

        if ($offer->additional_data != 'false') {
            $additionalData = json_decode($offer->additional_data);
            $refill = $additionalData->refill;
            $country = $additionalData->country;
            $refillPrice = 0;
            $targetPrice = 0;
            
            if ($refill == 3) {
                $refillPrice = 0;
            } elseif ($refill == 7) {
                $refillPrice = 3;
            } elseif ($refill == 14) {
                $refillPrice = 5;
            } elseif ($refill == 30) {
                $refillPrice = 9;
            }

            if ($country != 'random') {
                $targetPrice = 10;
            } else {
                $targetPrice = 0;
            }
            $additionalDataPrice = $refillPrice + $targetPrice;

        } else {
            $additionalDataPrice = 0;
        }
        if ($offer->special_offer == 'true') {
            $specialOfferPrice = $offer->pocket->specialOffer->total;
        } else {
            $specialOfferPrice = 0;
        }

        return $price = $pocketPrice + $specialOfferPrice + $additionalDataPrice;
    }

    public function validateToken(Request $request)
    {
        $token = $request->input('token');
        $transactions = UserTransaction::getTransactionByToken($token);
        $transaction = $this->getTransactionWhereStatusZero($transactions);
        
        if ($transaction) {
            $price = $this->getPriceByTransaction($transaction);
            $transaction->setTransactionStatus(1);
            return response(array('cost' => $price));
        } else {
            return response('error: transaction not found');
        }
    }

    public function messageController(Request $request)
    {
        $email = $request->input('email');
        $message = $request->input('message');
        $username = $request->input('username');
        $category = $request->input('subject');

        $userMessage = "Information message: you ticket add to system. Please wait answer. ";

        $subject = "=?utf8-8?B?".base64_encode("Ticket $username ".date("Y-m-d H:i:s"))."?=";
        $userSubject = "=?utf8-8?B?".base64_encode("Message from site BoosterBoom.com")."?=";

        $headers = "From: $email\nReply: $email\nContent-type: text/html; charset=utf8\n";
        $userHeaders = "From: support@getfolowers.com\nReply: support@getfolowers.com\nContent-type: text/html; charset=utf8\n";

        mail($email, $userSubject, $userMessage, $userHeaders);
        mail("support@BoosterBoom.com", $subject, $message, $headers);

        return "true";
    }
    
    private function getTransactionWhereStatusZero($recordSet)
    {
        foreach ($recordSet as $record) {
            if ($record->transaction_status == 0) {
                return $record;
            }
        }
    }

    public function showFaqView()
    {
        return view('faqPage');
    }
}

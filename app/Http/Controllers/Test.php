<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\UserOrder;

class Test extends Controller
{
    public function test(Request $request): void
    {
        
        $today = Carbon::now();
        $next = Carbon::now()->addMonth();
        $dif = $today->diffInDays($next);
        dd($today->today(), $today, $next, $dif);
    }
}

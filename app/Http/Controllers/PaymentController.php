<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use PayPal\Api\Amount;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Order;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

use App\Models\OrdersModel;
use App\Models\CategoryModel;
use App\Models\PaypalCashoutModel;

class PaymentController extends Controller
{
    private $apiContext;
    protected $items = [];
    protected $provider;
    protected $request;

    public function __construct()
    {
        /** PayPal api context **/
        $client_id = PaypalCashoutModel::where('isActive', '=', 1)->pluck('client_id');
        $secret = PaypalCashoutModel::where('isActive', '=', 1)->pluck('secret');
        $paypal_conf = \Config::get('paypal');
        $this->apiContext = new ApiContext(new OAuthTokenCredential(
                $client_id[0],
                $secret[0])
        );
        $this->apiContext->setConfig($paypal_conf['settings']);
    }



    public function index($type = null, $count = null, Request $request)
    {
        if ($request->isMethod('post')) {
            $this->payWithPayPal($request);
        }

        if (!is_null($type)) {
            $categories = CategoryModel::where("type", $type)->get()->toJson();
        } else {
            abort(404);
        }

        return view('blocks.pay', [
            "categories" => $categories,
            "type" => $type,
            "count" => $count
        ]);
    }

    public function successPayment() {
        $orderID = \session()->get('orderID');
        if (!is_null($orderID)) {
            $this->sendMailToUser($orderID);
            \session()->forget('success');
            return view('successPayment', ["orderID" => $orderID[0]]);
        } else {
            return view('successPayment', ['orderID' => 'pay failed']);
        }
        
    }

    private function getCategoryPrice(Request $request) {
        $price = CategoryModel::all()
            ->where('type', '=', $request->input('type'))
            ->where('count', '=', $request->input('count'))
            ->pluck('cost');
        return $price[0];
    }

    private function getCategoryID(Request $request) {
        $categoryID = CategoryModel::all()
            ->where('type', '=', $request->input('type'))
            ->where('count', '=', $request->input('count'))
            ->pluck('id');
        return $categoryID[0];
    }

    public function payWithPayPal(Request $request) {

        if ($request->isMethod('GET')) {
            return Redirect::route('success');
        }

        $price = $this->getCategoryPrice($request);
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $item_1 = new Item();

        $item_1
            ->setName($request->input('type'))
            ->setCurrency('USD')
            ->setQuantity(1)
            ->setPrice($price);


        $item_list = new ItemList();
        $item_list->setItems(array($item_1));


        $amount = new Amount();
        $amount->setCurrency('USD')
            ->setTotal($price);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('BoosterBoom.com');

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::route('status'))
            ->setCancelUrl(URL::route('status'));

        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));

        try {
            $payment->create($this->apiContext);
        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            if (\Config::get('app.debug')) {
                \Session::put('error', 'Connection timeout');
                return Redirect::route('/');
            } else {
                \Session::put('error', 'Some error occur, sorry for inconvenient');
                return Redirect::route('/');
            }
        }

        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        Session::put('paypal_payment_id', $payment->getId());

        $this->createOrder($request);
        

        if (isset($redirect_url)) {
            /** redirect to paypal **/
            //\Response::json($redirect_url);
            //return Redirect::away($redirect_url);
            return \Response::json(array('redirect_url' => $redirect_url));
        }

        \Session::put('error', 'Unknown error occurred');
        return Redirect::route('/');
    }

    public function getPaymentStatus(Request $request)
    {
        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');
        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
            \Session::put('error', 'Payment failed');
            return Redirect::route('index');
        }

        $payment = Payment::get($payment_id, $this->apiContext);
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));

        /**Execute the payment **/
        $result = $payment->execute($execution, $this->apiContext);

        if ($result->getState() == 'approved') {
            $order = new OrdersModel();

            $props = [
                'paypal_payment_id' => $request->input('paymentId'),
                'token' => $request->input('token'),
                'PayerID' => $request->input('PayerID'),
                'payment_status' => 1
            ];

            \Session::put('success', 'Payment success');
            $order->updateOrder($props, $order);

            $props = [
                'paypal_payment_id' => $request->input('paymentId')
            ];

            $order->addOrderToSMMPanel($props);
            return Redirect::route('index');
        }

        \Session::put('error', 'Payment failed');
        return Redirect::route('index');
    }

    private function createOrder($request) {
        $order = new OrdersModel();

        $props = [
            'user_id' => $request->input('user_id'),
            'email' => $request->input('email'),
            'category_id' => $this->getCategoryID($request),
            'paypal_payment_id' => $request->session()->get('paypal_payment_id'),
            'cost' => $this->getCategoryPrice($request),
            'link' => $request->input('link'),
            'nickname' => $request->input('nickname')
        ];
        //dd($props);

        $order->createOrder($props, $order);
    }
}

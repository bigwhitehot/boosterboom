<?php

namespace App\Http\Controllers;

use App\Models\UserOrderRequest;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UserOrderRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $userOrder = UserOrderRequest::create();

        dd($request);
        return array(
            'request' => $request,
            // 'userOrder' => $userOrder
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserOrderRequest  $userOrderRequest
     * @return \Illuminate\Http\Response
     */
    public function show(UserOrderRequest $userOrderRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserOrderRequest  $userOrderRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(UserOrderRequest $userOrderRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserOrderRequest  $userOrderRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserOrderRequest $userOrderRequest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserOrderRequest  $userOrderRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserOrderRequest $userOrderRequest)
    {
        //
    }
}

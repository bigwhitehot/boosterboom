<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\CategoryModel;
use Carbon\Carbon;

class TestOrders extends Model
{
    protected $table = 'test_orders';
    const ALLOW_TIME = 24;

    private function getDateDifferenceInHours(string $userDate): number
    {
        $nowTime = Carbon::now();
        $timetamp = Carbon::createFromTimeString($userDate);
    }

    public function isUserHaveNotGetThisPocket(CategoryModel $category, number $uid): boolean
    {
        $type = $category->type;
        $timestampString = TestModel::where('uid', $uid)->latest()->first()->create_at;
        $differenseInHours = $this->getDateDifferenceInHours($timestampStringß);
 
        if ($this->getDateDifferenceInHours > self::ALLOW_TIME) {
            return true;
        } else {
            
        }
    }


}

<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Watcher extends Model {

    protected $table = 'processing_orders';
    private $activeOrdersCollection;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    private function getActiveOrdersCollection() {
        return Watcher::all();
    }

    private function getOrderIdCollection() {
        $orderIdCollection = [];
        foreach (Watcher::all() as $order) {
            array_push($orderIdCollection, $order->order_id);
        }
        return $orderIdCollection;
    }

    private function updateActiveOrdersCollection() {
        foreach (Watcher::all() as $order) {
            $status = $this->getStatusOrder($order->order_id);
            $order->status = $status->status;
            $order->remains = $status->remains;
            $order->start_count = $order->start_count + 1;
            $order->save();
        }
    }

    private function getStatusOrder($orderID) {
        $order = Watcher::where('order_id', $orderID)->get();
        $status = new SmmPanelAPIModel($order[0]->panel);
        return $status->status($orderID);
    }

    private function getOrderNumber($order) {
        return $order->order_id;
    }

    public function showProcessingOrders() {
        return $this->activeOrdersCollection;
    }

    public function checkOrdersList() {

        foreach (Watcher::all() as $order) {
            switch ($order->status) {
                case "Completed": {

                    $attributes = [
                        "start_count" => $order->start_count,
                        "quantity" => $order->quantity,
                        "pppid" => $order->pppid,
                        "order_id" => $order->order_id,
                        "link" => $order->link
                    ];
                    $complete = new CompletedOrders($attributes);
                    $complete->save();
                    $order->delete();
                }
            }
        }
    }


    public function test() {
        $this->checkOrdersList();
    }
}
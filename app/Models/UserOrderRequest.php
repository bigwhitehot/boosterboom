<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserOrderRequest extends Model
{
    protected $table = 'user_order_requests';

    public function category()
    {
        return $this->hasOne('App\Models\CategoryModel');
    }
}

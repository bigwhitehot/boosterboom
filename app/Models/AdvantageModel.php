<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

class AdvantageModel extends Model
{
    protected $table = 'advantages';

    public static function getAdvantagesNameList(): Collection
    {
        $namesList = collect();
        (new static)->get()->each(function(self $item, int $i) use ($namesList) {
            $namesList[$i] = $item->name;
        });

        return new Collection($namesList);
    }
}

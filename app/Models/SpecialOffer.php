<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\CategoryModel;

class SpecialOffer extends Model
{
    public static function getPocketByType($type)
    {
        return SpecialOffer::where('type', '=', $type)->get();
    }

    public function category()
    {
        return $this->hasOne(CategoryModel::class, 'special_offer_id');
    }
}

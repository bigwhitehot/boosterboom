<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AvailableServices extends Model
{
    protected $table = 'available_services';
}

<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class CompletedOrders extends Model
{
    protected $table = 'completed_orders';
    protected $guarded = ["id"];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function showCompletedOrders()
    {
        return CompletedOrders::all();
    }
}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Proxy extends Model
{
    protected $table = 'proxies';

    public static function getActiveProxy()
    {
        return self::where('isActive', 1)->first();
    }
}

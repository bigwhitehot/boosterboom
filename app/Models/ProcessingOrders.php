<?php

namespace App\Models;

use App\Models\Mail;
use Illuminate\Database\Eloquent\Model;

class ProcessingOrders extends Model
{
    protected $table = 'processing_orders';
    protected $fillable = ['charge',
        'pppid',
        'order_id',
        'panel',
        'link',
        'status',
        'start_count',
        'service',
        'quantity',
        'remains'
    ];

    private $pppid;
    private $panel;
    private $orderID;
    private $link;
    private $quantity;

    public function __construct(array $attributes = []) {
        $this->pppid = $attributes['paypal_payment_id'];
        $this->orderID = $attributes['orderID'];
        $this->panel = $attributes['panel'];
        $this->quantity = $attributes['quantity'];
        $this->link = $attributes['link'];
        parent::__construct();
    }

    public function getPayPalPaymentID() {
        return $this->pppid;
    }

    public function getPanel() {
        return $this->panel;
    }

    public function getLink() {
        return $this->link;
    }

    public function getOrderID() {
        return $this->orderID;
    }

    public function getQuantity() {
        return $this->quantity;
    }

    public function getOrderData(){
        $object = new SmmPanelAPIModel(env('CURRENT_SMMPANEL'));
        $orderData = $object->status($this->orderID);
        return $orderData;
    }

    private function getUserEmail() {

    }

    public function addProcessingOrderToDB(ProcessingOrders $order) {
        $orderData = $this->getOrderData();
        $pppid = $this->pppid;
        $id = [];
        $id = preg_split('/PAYID-/', $pppid);

        session()->push('orderID', $this->getOrderID());

        $order->fill(array(
            'charge' => $orderData->charge,
            'start_count' => $orderData->start_count,
            'status' => $orderData->status,
            'remains' => $orderData->remains,
            'link' => $this->getLink(),
            'quantity' => $this->getQuantity(),
            'pppid' => $this->getPayPalPaymentID(),
            'panel' => $this->getPanel(),
            'order_id' => $this->getOrderID()
        ));
        $order->save();
    }
}

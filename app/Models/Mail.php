<?php

    namespace App\Model;

    class Mail extends Model
    {

        protected $table = "mails";
        private $emailReceiver, $message;
        public function __construct($emailReceiver, $message)
        {
            $this->message = $message;
            $this->emailReceiver = $emailReceiver;
        }


        public function sendMessage() {

            $headers = "From: support@getfolowers.com\nReply: support@getfolowers.com\nContent-type: text/html; charset=utf8\n";
            $subject = "=?utf8-8?B?".base64_encode("Message from site BoosterBoom.com")."?=";

            mail($this->emailReceiver, $subject, $this->message, $headers);
        }
    }
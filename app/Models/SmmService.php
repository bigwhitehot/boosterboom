<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\SmmPanel;
use App\Models\SmmOrder;
use App\Models\SmmService;

class SmmService extends Model
{
    public function smmPanel()
    {
        return $this->belongsTo(SmmPanel::class, 'smm_panel_id');
    }

    public function smmOrders()
    {
        return $this->hasMany(SmmOrder::class);
    }

    public static function getServicesBySmmPanelId($smmPanelId)
    {
        return SmmService::where('smm_panel_id', '=', $smmPanelId)
            ->get();
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \App\Models\AdvantageModel;

use App\Models\UserOrder;
use App\Models\SpecialOffer;
use App\Models\PocketService;

class CategoryModel extends Model
{
    protected $table = 'category';

    public function order() {
        return $this->hasMany(OrdersModel::class, 'category_id');
    }

    public function specialOffer()
    {
        return $this->belongsTo(SpecialOffer::class, 'special_offer_id');
    }

    public function pocketService()
    {
        return $this->hasOne(PocketService::class, 'pocket_id');
    }

    public function userOffer()
    {
        return $this->hasOne(UserOrder::class, 'pocket_id');
    }

    public function getListAdvantages()
    {
        $advantages = [];
        foreach (AdvantageModel::all() as $row) {
            $advantages[$row->id] = $row->name;
        }

        return $advantages;
    }

    public function setListAdvantagesAttribute($value)
    {
        $this->attributes['listAdvantages'] = json_encode($value);
    }

    public function getListAdvantagesAttribute($value = false)
    {
        $this->attributes['listAdvantages'] = AdvantageModel::findMany(json_decode($value))->pluck('name');
    }
    
    public function getListAdvantagesAttributeValue()
    {
        return $this->attributes["listAdvantages"];
    }
    
    public function getListAdvantagesAsArray()
    {
        return AdvantageModel::findMany(json_decode($this->getListAdvantagesAttributeValue()))->pluck('name');
    }
    
    public function getLeftPointCost()
    {
        return preg_replace("/\..*/", "", $this->getCost());
    }
    
    /**
     * 
     * @return string
     */
    public function getRightPointCost()
    {
        return preg_replace("/.*\./", "", $this->getCost());
    }
    
    public function getCost()
    {
        return number_format($this->getAttribute("cost"), 2);
    }
}
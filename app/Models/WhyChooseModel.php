<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WhyChooseModel extends Model
{
    protected $table = 'why_choose';
}

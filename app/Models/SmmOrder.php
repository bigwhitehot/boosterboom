<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\UserOrder;
use App\Models\SmmService;

class SmmOrder extends Model
{
    protected $table = 'smm_orders';
    protected $guarded = [];

    public function userOrder()
    {
        return $this->belongsTo(UserOrder::class);
    }

    public function SmmService()
    {
        return $this->belongsTo(SmmService::class);
    }
}

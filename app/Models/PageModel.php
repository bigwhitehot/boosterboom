<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class PageModel extends Model
{

    protected $table = 'pages';

    public function getListBlocks()
    {
        $blocks = [];
        foreach (BlockModel::all() as $row) {
            $blocks[$row->id] = $row->name;
        }
        
        return $blocks;
    }
    
    public function setListBlocksAttribute($value)
    {
        $this->attributes['listBlocks'] = json_encode($value);
    }
    
    public function getListBlocksModels()
    {
        return BlockModel::findMany(json_decode($this->getListBlocksAttributeValue()));
    }
    
    public function getListBlocksAttributeValue()
    {
        return $this->attributes["listBlocks"];
    }
    
    public function getListBlockesAsArray()
    {
        return BlockModel::findMany(json_decode($this->getListBlocksAttributeValue()))->pluck('name');
    }
    
    public function getBlockModelsArrayOptions($key)
    {
        $modifyKey = ucfirst($key);
        if (method_exists($this, "getBlockModelsOptions{$modifyKey}")) {
            return $this->{"getBlockModelsOptions{$modifyKey}"}();
        }
        return [];
    }

    public function getBlockModelsOptionsCategory()
    {
        $testPockets = $this->getTestPocketsCollection();

        if ($this->pageName == null) {
            $pockets = $testPockets->merge(CategoryModel::where(['type' => 'special'])->get());
            return ["categories" => $pockets, "advantages" => AdvantageModel::getAdvantagesNameList()];
        } else {
            $pockets = $testPockets->merge(CategoryModel::where(['type' => $this->pageName])->get());
            return ["categories" => $pockets, "advantages" => AdvantageModel::getAdvantagesNameList()];
        }
    }

    private function getTestPocketsCollection(): Collection
    {
        $testPockets = collect();
        CategoryModel::get()->each(function (CategoryModel $item) use ($testPockets) {
            if (preg_match_all('/Test/i', $item->description)) {
                $testPockets->push($item);
            }
        });
        return new Collection($testPockets);
    }

    public function setPageNameAttribute($value)
    {
        $this->attributes['pageName'] = $value;
    }
}

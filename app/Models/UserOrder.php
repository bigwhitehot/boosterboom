<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\SmmOrder;
use App\Models\UserTransaction;
use App\Models\CategoryModel;

class UserOrder extends Model
{
    protected $table = 'user_orders';
    protected $guarded = [];

    public function userTransaction()
    {
        return $this->hasOne(UserTransaction::class);
    }

    public function smmOrder()
    {
        return $this->hasMany(SmmOrder::class);
    }

    public function pocket()
    {
        return $this->belongsTo(CategoryModel::class);
    }

    public function updateOrder($options)
    {
        $this->update($options);
    }
}

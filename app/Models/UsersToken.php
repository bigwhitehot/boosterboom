<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class UsersToken extends Model
{
    //
    protected $table = 'users_tokens';

    protected $tokenLifeTime = 5 * 60;

    private $token;
    private $session;

    protected $fillable = ['token', 'session_id'];

    private static function getUserTokenCollection($token)
    {   
        return UsersToken::where('token', '=', $token)->first();
    }

    public static function getToken($token)
    {
        return UsersToken::getUserTokenCollection($token)
            ->toArray()['token'];
    }

    public static function getLifeTimeToken($token)
    {
        $userTokenCollection = UsersToken::getUserTokenCollection($token);
        $lifeTimeToken = $userTokenCollection->toArray()['created_at'];
        // if after created_at got more then 300 seconds, then token not valid
        // return $lifeTimeToken;
        return Carbon::createFromFormat('D-M-Y H:i:s e+', $lifeTimeToken)->diffInSeconds();
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\UserOrder;
use App\Models\SmmService;
use App\Models\SmmOrder;
use App\Services\SmmPanelAPI;

class SmmPanel extends Model
{
    protected $table = 'smm_panels';
    protected $guarded = [];
    const MIN_BALANCE = 0.0;

    public static function refreshBalances() {
        $panels = SmmPanel::all();
        foreach ($panels as $panel) {
            $balance = floatval((new SmmPanelApi($panel))->balance()->balance);
            if ($balance >= self::MIN_BALANCE ) {
                $panel->update(array('isActive' => 1));
            } else {
                $panel->update(array('isActive' => 0));
            }
        }
    }

    public static function getActiveSmmPanels()
    {
        $records = SmmPanel::where('isActive', '=', 1)
            ->get();
        if ($records) {
            return $records;
        } else {
            abort(404, 'No have Active SMM panel', SmmPanel::class);
        }
    }

    public function userOrder()
    {
        return $this->belongsTo(UserOrder::class);
    }

    public function SmmServices()
    {
        return $this->hasMany(SmmService::class);
    }

    public function smmOrders()
    {
        return $this->hasMany(SmmOrder::class);
    }

    public function getServicesByType($type)
    {
        $services = $this->SmmServices;
        $typeServices = [];
        foreach ($services as $service) {
            if ($service->type == $type) {
                array_push($typeServices, $service);
            } else {
                continue;
            }
        }
        return $typeServices;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\UserOrder;

class UserTransaction extends Model
{

    const STATUS = 0;
    const LIFETIME = 24 * 60;

    protected $table = 'user_transactions';
    protected $fillable = ['session_id', 'token', 'uid'];
    protected $attributes = ['transaction_status' => self::STATUS, 'lifetime' => self::LIFETIME];

    public function userOrder()
    {
        return $this->belongsTo(UserOrder::class);
    }

    public function updateTransaction($options)
    {
        $this->update($options);
    }

    public function setTransactionStatus($status)
    {
        $this->transaction_status = $status;
        $this->save();
    }

    public function setIsAbort($status)
    {
        $this->isAbort = $status;
        $this->save();
    }

    public static function getTransactionByToken($token)
    {
        return self::where('uid', '=', $token)
            ->get();
    }

    public static function getLatestTransactionByToken($token)
    {
        return UserTransaction::where('uid', '=', $token)
            ->latest()
            ->first();
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExecuteOrder extends Model
{
    protected $table = 'execute_orders';
    protected $guarded = [];
}

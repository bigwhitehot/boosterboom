<?php
namespace App\Models;

use function GuzzleHttp\Promise\all;
use Illuminate\Database\Eloquent\Model;
use PayPal\Api\Order;


class OrdersModel extends Model {

    protected $table = 'orders';
    protected $primaryKey = 'id';

    public function category() {
        return $this->belongsTo(CategoryModel::class);
    }

    public function createOrder($props, OrdersModel $order) {


        $order->user_id = $props['user_id'];
        $order->email = $props['email'];
        $order->category_id = $props['category_id'];
        $order->paypal_payment_id = $props['paypal_payment_id'];
        $order->cost = $props['cost'];
        $order->link = json_encode($props['link']);
        $order->nickname = $props['nickname'];
        $order->save();

    }

    public function updateOrder($props, OrdersModel $query) {
        $query->where('paypal_payment_id', $props['paypal_payment_id'])
            ->update([
                'payer_id' => $props['PayerID'],
                'token' => $props['token'],
                'payment_status' => $props['payment_status']
            ]);
    }

    public function getCategoryCount($pppid) {
        return $this->getCategoryCollection($pppid)->count;
    }

    private function getCategoryType($pppid) {
        return $this->getCategoryCollection($pppid)->type;
    }

    private function getCategoryCollection($pppid) {
        $order = OrdersModel::where('paypal_payment_id', '=', $pppid)->first();
        return CategoryModel::where('id', $order->category_id)->first();
    }

    public function getNickname($pppid) {
        return OrdersModel::where('paypal_payment_id', '=', $pppid)->pluck('nickname')[0];
    }


    private function addLikesOrder($props) {
        $order = new SmmPanelAPIModel();
        $orderIds = array();
        $imagesUrls = json_decode(OrdersModel::where('paypal_payment_id', '=', $props['paypal_payment_id'])->value('link'));
        $service = AvailableServices::all()->where('category', '=', 'likes')
            ->sortBy('metrica')->first();
        foreach ($imagesUrls as $imagesUrl) {
            $link = "https://www.instagram.com/p/" . $imagesUrl . "/";
            $orderID = $order->order(array(
                'service' => $service['service_id'],
                'link' => $link,
                'quantity' => $this->getCategoryCount($props['paypal_payment_id'])
            ));
            array_push($orderIds, $orderID->order);
            $params = [];
            $params['link'] = "https://www.instagram.com/" . $imagesUrl;
            $params['paypal_payment_id'] = $props['paypal_payment_id'];
            $params['quantity'] = $this->getCategoryCount($props['paypal_payment_id']);
            $params['panel'] = $order->smm_name;
            $params['orderID'] = $orderID->order;
            $processingOrder = new ProcessingOrders($params);
            $processingOrder->addProcessingOrderToDB($processingOrder);
        }
    }

    private function addFollowersOrder($props) {
        $order = new SmmPanelAPIModel();

        $service = AvailableServices::all()->where('category', '=', 'followers')
            ->sortBy('metrica')->first();
        if (!is_null($service)) {
            $orderID = $order->order(array(
                'service' => $service['service_id'],
                'link' => "https://www.instagram.com/".$this->getNickname($props['paypal_payment_id']),
                'quantity' => $this->getCategoryCount($props['paypal_payment_id'])
            ));
            $params = [];
            $params['link'] = "https://www.instagram.com/".$this->getNickname($props['paypal_payment_id']);
            $params['paypal_payment_id'] = $props['paypal_payment_id'];
            $params['quantity'] = $this->getCategoryCount($props['paypal_payment_id']);
            $params['panel'] = $order->smm_name;
            $params['orderID'] = $orderID->order;
            $processingOrder = new ProcessingOrders($params);
            $processingOrder->addProcessingOrderToDB($processingOrder);
        } else {
            return view('successPayment', ['orderID' =>'Добавьте сервис с панели']);
        }
    }

    private function addViewsOrder($props) {
        $order = new SmmPanelAPIModel();
        $imagesUrls = json_decode(OrdersModel::where('paypal_payment_id', '=', $props['paypal_payment_id'])->value('link'));
        $service = AvailableServices::all()->where('category', '=', 'views')
            ->sortBy('metrica')->first();
        foreach ($imagesUrls as $imagesUrl) {
            $link = "https://www.instagram.com/p/". $imagesUrl ."/";
            $orderID = $order->order(array(
                'service' => $service['service_id'],
                'link' => $link,
                'quantity' => $this->getCategoryCount($props['paypal_payment_id'])
            ));
            $params = [];
            $params['link'] = "https://www.instagram.com/p/". $imagesUrl ."/";
            $params['paypal_payment_id'] = $props['paypal_payment_id'];
            $params['quantity'] = $this->getCategoryCount($props['paypal_payment_id']);
            $params['panel'] = $order->smm_name;
            $params['orderID'] = $orderID->order;
            $processingOrder = new ProcessingOrders($params);
            $processingOrder->addProcessingOrderToDB($processingOrder);
        }
    }

    private function addAutolikesOrder($props) {
        $order = new SmmPanelAPIModel();
        $service = AvailableServices::all()->where('category', '=', 'autolikes')
            ->sortBy('metrica')->first();
        $orderID = $order->order(array(
            'service' => $service['service_id'],
            'link' => "https://www.instagram.com/".$props['username'],
            'quantity' => $props['count']
        ));
        $params = [];
        $params['link'] = "https://www.instagram.com/".$props['username'];
        $params['paypal_payment_id'] = $props['paypal_payment_id'];
        $params['quantity'] = $props['count'];
        $params['panel'] = $order->smm_name;
        $params['orderID'] = $orderID;
        $processingOrder = new ProcessingOrders($params);
        $processingOrder->addProcessingOrderToDB($processingOrder);
    }

    public function addOrderToSMMPanel($props){
        $category = $this->getCategoryType($props['paypal_payment_id']);

        if ($category == 'likes') {
            $this->addLikesOrder($props);

        } elseif ($category == 'followers') {
            $this->addFollowersOrder($props);

        } elseif ($category == 'views') {
            $this->addViewsOrder($props);

        } elseif ($category == 'autolikes') {
            $this->addAutolikesOrder($props);
        }
    }
}
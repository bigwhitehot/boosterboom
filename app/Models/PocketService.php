<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\CategoryModel;
use App\Models\SmmService;

class PocketService extends Model
{
    protected $table = 'pocket_services';
    protected $guarded = [];
    
    private $seviceId;
    private $pocketId;

    public static function getServicesCollectionById($id)
    {
        return self::where('pocket_id', '=', $id)->get();
    }

    public function service()
    {
        return $this->belongsTo(SmmService::class);
    }

    public function pocket()
    {
        return $this->belongsTo(CategoryModel::class);
    }

    private function getService(CategoryModel $pocket)
    {
        return $this->serviceId = $service->id;
    }

    private function getPocket(CategoryModel $pocket)
    {
        return $this->pocketId = $pocket->id;
    }
}

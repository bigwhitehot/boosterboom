<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Test extends Model
{
    protected $table = 'test'; //определение таблицы
    protected $primaryKey = 'id'; //первичный ключ
    //public $timestamps = true; //автоматическое управление временными метками
}

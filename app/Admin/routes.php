<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index');
    $router->resource('category/index', CategoryController::class);
    $router->resource('advantage/index', AdvantageController::class);
    $router->resource('reviews/index', ReviewsController::class);
    $router->resource('page/index', PageController::class);
    $router->resource('menu/index', MenuController::class);
    $router->resource('block/index', BlockController::class);
    $router->resource('why-choose/index', WhyChooseController::class);
    $router->resource('cash', CashController::class);
    $router->resource('services', AvailableServiceController::class);
    $router->resource('paypal-cashout', PaypalCashoutController::class);
    $router->resource('smmpanel', SmmPanelController::class);
    $router->resource('proxies', ProxyController::class);
    $router->resource('pocket-services', PocketServiceController::class);
    $router->resource('smm-services', SmmServiceController::class);
    $router->resource('user-orders', UserOrderController::class);
    $router->resource('user-transactions', UserTransactionController::class);
    $router->resource('special-offers', SpecialOfferController::class);
});

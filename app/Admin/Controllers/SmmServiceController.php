<?php

namespace App\Admin\Controllers;

use App\Models\SmmService;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class SmmServiceController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'App\Models\SmmService';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new SmmService);

        $grid->column('id', __('Id'));
        $grid->column('type', __('Тип'));
        $grid->column('smm_panel_id', __('Smm panel id'));
        $grid->column('service_id', __('Service id'));
        $grid->column('rate', __('Rate'));
        $grid->column('min_quantity', __('Min quantity'));
        $grid->column('max_quantity', __('Max quantity'));
        $grid->column('start_time', __('Start time'));
        $grid->column('speed_per_day', __('Speed per day'));
        $grid->column('refill', __('Refill'));
        $grid->column('description', __('Description'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(SmmService::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('type', __('Type'));
        $show->field('smm_panel_id', __('Smm panel id'));
        $show->field('service_id', __('Service id'));
        $show->field('rate', __('Rate'));
        $show->field('min_quantity', __('Min quantity'));
        $show->field('max_quantity', __('Max quantity'));
        $show->field('start_time', __('Start time'));
        $show->field('speed_per_day', __('Speed per day'));
        $show->field('refill', __('Refill'));
        $show->field('description', __('Description'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new SmmService);

        // $form->textarea('type', __('Type'))->default('followers');
        $form->select('type', __('Тип'))->options([1 => 'followers', 2 => 'likes', 3 => 'views']);
        $form->number('smm_panel_id', __('ID СММ Панели'));
        $form->number('service_id', __('id Сервиса'));
        $form->decimal('rate', __('Цена за 1000'));
        $form->number('min_quantity', __('Минимальное количество на ордер'))->default(10);
        $form->number('max_quantity', __('Максимальное количество на ордерy'));
        $form->number('start_time', __('Время для старта'));
        $form->number('speed_per_day', __('количество в день'));
        $form->textarea('refill', __('Refill'));
        $form->textarea('description', __('Описание'));

        return $form;
    }
}

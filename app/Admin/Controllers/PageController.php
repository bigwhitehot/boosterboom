<?php

namespace App\Admin\Controllers;

use App\Models\PageModel;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Http\Request;

class PageController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     * 
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Show')
            ->description('Detail info')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        $form = $this->form();
        $form = $form->edit($id);
        $form->builder()->field('listBlocks')->value(json_decode($form->model()->getAttributes()["listBlocks"]));
        return $content
            ->header('Edit')
            ->description('description')
            ->body($form);
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (is_null($request->listBlocks[sizeof($request->listBlocks)-1])) {
            $request->listBlocks = json_encode(array_except($request->listBlocks, sizeof($request->listBlocks)-1));
        }
        return $this->form()->update($id);
    }
    
    
    /**
     * Store a newly created resource in storage.
     *
     * @return mixed
     */
    public function store(Request $request)
    {
        if (is_null($request->listBlocks[sizeof($request->listBlocks)-1])) {
            $request->listBlocks = json_encode(array_except($request->listBlocks, sizeof($request->listBlocks)-1));
        }
        return $this->form()->store();
    }
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new PageModel);

        $grid->id('id');
        $grid->column("title", "Заголовок");
        $grid->created_at('Created at');
        $grid->updated_at('Updated at');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(PageModel::findOrFail($id));

        $show->id('id');
        $show->url('url');
        $show->title('title');
        $show->metaDescription('metaDescription');
        $show->metaKeywords('metaKeywords');
        $show->summernote('content');
        $show->listAdvantages("listBlocks", "Список блоков");
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new PageModel);

        $form->display('ID');
        $form->text('url');
        $form->text('title');
        $form->text('metaDescription');
        $form->text('metaKeywords');
        $form->summernote('content');
        $form->multipleSelect('listBlocks')->options(\App\Models\BlockModel::all()->pluck('title', 'id'));
        $form->display('Created at');
        $form->display('Updated at');

        return $form;
    }
}

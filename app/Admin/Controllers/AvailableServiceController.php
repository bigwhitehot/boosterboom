<?php

namespace App\Admin\Controllers;

use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Grid;
use Encore\Admin\Form;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Show;

use App\Models\AvailableServices;

class AvailableServiceController extends Controller
{
    use HasResourceActions;
    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            -> title('Services')
            -> description('Available Instagram Services')
            -> body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content){
        return $content->description('Show details')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content) {
        $form = $this->form();
        $form = $form->edit($id);
        //$form->builder()->field('name')->value(json_decode($form->model()->getAttributes()["name"]));
        return $content
            ->header('Edit')
            ->body($form);
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content) {
        return $content->title('Create new service')
            ->body($this->form());
    }

    public function update($id) {
        return $this->form()->update($id);
    }

    public function store() {
        return $this->form()->store();
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid() {
        $grid = new Grid(new AvailableServices);

        $grid->quickSearch('service_id');
        $grid->disableFilter();

        $grid->id('id');
        $grid->column('panel');
        $grid->column('service_id');
        $grid->column('metrica');
        $grid->column('name');
        $grid->column('category');
        $grid->column('cost');

        $grid->created_at();
        $grid->updated_at();

        $grid->disableRowSelector();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id) {
        $show = new Show(AvailableServices::findOrFail($id));

        $show->field('id');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form() {
        $form = new Form(new AvailableServices);

        $form->text('panel')->default('JAP');
        $form->text('service_id');
        $form->select('name')->options([1 => 'JAP', 2 => 'POSTLIKES']);
        $form->select('category')->options(
            [
                'autolikes' => 'autolikes',
                'comments' => 'comments', 
                'views' => 'views',
                'followers' => 'followers',
                'likes' => 'likes'
            ],
        );
        $form->text('cost');
        $form->text('description');
        $form->text('metrica');
        $form->text('start_time');
        $form->text('speed_per_day');
        $form->text('refil');
        $form->text('min');
        $form->text('max');


        $form->footer(function ($footer) {
            $footer->disableViewCheck();
            $footer->disableEditingCheck();
            $footer->disableCreatingCheck();
        });

        return $form;
    }
}

<?php

namespace App\Admin\Controllers;

use App\Models\UserTransaction;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class UserTransactionController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'App\Models\UserTransaction';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new UserTransaction);

        $grid->column('id', __('Id'));
        $grid->column('uid', __('Uid'));
        $grid->column('session_id', __('Session id'));
        $grid->column('user_order_id', __('User order id'));
        $grid->column('token', __('Token'));
        $grid->column('lifetime', __('Lifetime'));
        $grid->column('transaction_status', __('Transaction status'));
        $grid->column('isAbort', __('IsAbort'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(UserTransaction::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('uid', __('Uid'));
        $show->field('session_id', __('Session id'));
        $show->field('user_order_id', __('User order id'));
        $show->field('token', __('Token'));
        $show->field('lifetime', __('Lifetime'));
        $show->field('transaction_status', __('Transaction status'));
        $show->field('isAbort', __('IsAbort'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new UserTransaction);

        $form->textarea('uid', __('Uid'));
        $form->textarea('session_id', __('Session id'));
        $form->number('user_order_id', __('User order id'));
        $form->textarea('token', __('Token'));
        $form->number('lifetime', __('Lifetime'))->default(1440);
        $form->switch('transaction_status', __('Transaction status'));
        $form->switch('isAbort', __('IsAbort'));

        return $form;
    }
}

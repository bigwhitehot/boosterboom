<?php

namespace App\Admin\Controllers;

use App\Models\Proxy;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class ProxyController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Управление прокладками';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Proxy);

        $grid->column('id', __('Id'));
        $grid->column('url', __('Url'));
        $grid->column('key', __('Key'));
        $grid->column('isActive', __('IsActive'));
        $grid->column('count', __('Count'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Proxy::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('url', __('Url'));
        $show->field('key', __('Key'));
        $show->field('isActive', __('IsActive'));
        $show->field('count', __('Count'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Proxy);

        $form->url('url', __('Url'));
        $form->text('key', __('Key'));
        $form->switch('isActive', __('IsActive'));
        $form->number('count', __('Count'));

        return $form;
    }
}

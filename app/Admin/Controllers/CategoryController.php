<?php

namespace App\Admin\Controllers;

use App\Models\CategoryModel;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        $form = $this->form();
        $form = $form->edit($id);
        $form->builder()->field('listAdvantages')->value(json_decode($form->model()->getAttributes()["listAdvantages"]));
        return $content
            ->header('Edit')
            ->description('description')
            ->body($form);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (is_null($request->listAdvantages[sizeof($request->listAdvantages)-1])) {
            $request->request->set('listAdvantages', array_except($request->listAdvantages, sizeof($request->listAdvantages)-1));
        }
        return $this->form()->update($id);
    }
    
    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return mixed
     */
    public function store(Request $request)
    {
        if (is_null($request->listAdvantages[sizeof($request->listAdvantages)-1])) {
            $request->listAdvantages = json_encode(array_except($request->listAdvantages, sizeof($request->listAdvantages)-1));
        }
        return $this->form()->store();
    }
    
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new CategoryModel);

        $grid->id('ID');
        $grid->name('Name');
        $grid->column('count', 'Count');
        $grid->select('cost', 'Cost')->options('multiple');
        $grid->created_at('Created at');
        $grid->updated_at('Updated at');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(CategoryModel::findOrFail($id));

        $show->id('ID');
        $show->name('name');
        $show->description('cost');
        $show->cost('cost');
        $show->count('cost');
        $show->listAdvantages("listAdvantages", "Преимущества");
        $show->linkOrder('linkOrder');
        $show->toMain('To main');
        $show->isFeatures('isFeatures');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        //$mCategory = new CategoryModel;
        $form = new Form(new CategoryModel);
        $form->display('ID');
        $form->text('name', 'Name = count');
        $form->text('description');
        $form->text('count');
        $form->text('type');
        $form->text('cost');
        $form->multipleSelect('listAdvantages')->options(\App\Models\AdvantageModel::all()->pluck('name', 'id'));
        $form->text('linkOrder')->default('/checkout/');
        $form->text('toMain')->default(0);
        $form->text('isFeatures');
        $form->text('special_offer');
        $form->display('Created at');
        $form->display('Updated at');

        return $form;
    }


}

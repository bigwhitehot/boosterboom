<?php

namespace App\Admin\Controllers;

use App\Models\PocketService;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class PocketServiceController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Управление Ассоциативной таблице';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new PocketService);

        $grid->column('id', __('Id'));
        $grid->column('pocket_id', __('Pocket id'));
        $grid->column('service_id', __('Service id'));
        $grid->column('refil', __('Refil'));
        $grid->column('country', __('Country'));
        $grid->column('description', __('Description'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(PocketService::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('pocket_id', __('Pocket id'));
        $show->field('service_id', __('Service id'));
        $show->field('refil', __('Refil'));
        $show->field('country', __('Country'));
        $show->field('description', __('Description'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new PocketService);

        $form->number('pocket_id', __('Pocket id'));
        $form->number('service_id', __('Service id'));
        $form->number('refil', __('Refil'));
        $form->text('country', __('Country'));
        $form->textarea('description', __('Description'));

        return $form;
    }
}

<?php

namespace App\Admin\Controllers;

use App\Models\SpecialOffer;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class SpecialOfferController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'App\Models\SpecialOffer';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new SpecialOffer);

        $grid->column('id', __('Id'));
        $grid->column('type', __('Type'));
        $grid->column('cost', __('Cost'));
        $grid->column('total', __('Total'));
        $grid->column('amount', __('Amount'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(SpecialOffer::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('type', __('Type'));
        $show->field('cost', __('Cost'));
        $show->field('total', __('Total'));
        $show->field('amount', __('Amount'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new SpecialOffer);

        $form->textarea('type', __('Type'));
        $form->decimal('cost', __('Cost'));
        $form->decimal('total', __('Total'));
        $form->number('amount', __('Amount'));

        return $form;
    }
}

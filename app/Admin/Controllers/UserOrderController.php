<?php

namespace App\Admin\Controllers;

use App\Models\UserOrder;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class UserOrderController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'App\Models\UserOrder';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new UserOrder);

        $grid->column('id', __('Id'));
        $grid->column('user_id', __('User id'));
        $grid->column('username', __('Username'));
        $grid->column('email', __('Email'));
        $grid->column('pocket_id', __('Pocket id'));
        $grid->column('special_offer', __('Special offer'));
        $grid->column('links', __('Links'));
        $grid->column('additional_data', __('Additional data'));
        $grid->column('smm_panel', __('Smm panel'));
        $grid->column('payed', __('Payed'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(UserOrder::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('user_id', __('User id'));
        $show->field('username', __('Username'));
        $show->field('email', __('Email'));
        $show->field('pocket_id', __('Pocket id'));
        $show->field('special_offer', __('Special offer'));
        $show->field('links', __('Links'));
        $show->field('additional_data', __('Additional data'));
        $show->field('smm_panel', __('Smm panel'));
        $show->field('payed', __('Payed'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new UserOrder);

        $form->text('user_id', __('User id'));
        $form->text('username', __('Username'));
        $form->email('email', __('Email'));
        $form->number('pocket_id', __('Pocket id'));
        $form->text('special_offer', __('Special offer'));
        $form->text('links', __('Links'));
        $form->text('additional_data', __('Additional data'));
        $form->number('smm_panel', __('Smm panel'));
        $form->text('payed', __('Payed'))->default('nothing');

        return $form;
    }
}
